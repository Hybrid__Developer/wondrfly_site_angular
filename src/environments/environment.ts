
export const environment = {
  production: false,
  apiUrls: {
    reports: 'https://wondrfly.com/api',
    // master: 'https://wondrfly.ml/api',
    master: 'https://wondrfly.com/api',

  },
  socketUrl: 'https://wondrfly.com:8406',
  // blogsUrl:  'https://blogs.wondrfly.ml',
  blogsUrl: 'https://cms.wondrfly.com',
  baseUrl: 'https://wondrfly.com/',
  shareUrl: 'https://wondrfly.com/#/',

  name: 'dev'

};


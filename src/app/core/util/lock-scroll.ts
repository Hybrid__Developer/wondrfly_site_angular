const $body = document.querySelector('body');

export default {
    enable() {
        $body.style.overflow = 'hidden';
    },
    disable() {
        $body.style.removeProperty('overflow');
    }
};

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { Role } from '../models/role.model';
import { ApiService } from '../services/api.service.service';
import { AuthsService } from '../services/auths.service';
@Injectable()
export class UserGuard implements CanActivate {
  constructor(private auth: AuthsService,
    private myRoute: Router, private cookie: CookieService, private activatedroute: ActivatedRoute, private apiservice: ApiService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // this.activatedroute.queryParams.subscribe(params => {
    //   let id = params['id'];
    //   if (window.location.pathname === '/login-parent' && id) {
    //     this.auth.getUserByIdInvitationLink(id)
    //   }
    // })
    let params = (new URLSearchParams(window.location.search));
    let id = params.get("id");
    if ((window.location.pathname === '/login-parent' && id)) {
      this.cookie.set('wondrlink', id)
      this.auth.getUserByIdInvitationLink(id)
    }
    else {
      if (!this.auth.currentUser() || !this.auth.isAuthorized()) {
        this.myRoute.navigate(["login"]);
      }
      const roles = next.data.roles as Role[];
      if (roles && !roles.some(r => this.auth.hasRole(r))) {
        this.myRoute.navigate(["login"]);
        return false;
      }

    }

    return true;
  }
}
@Injectable()
export class ActiveUser implements CanActivate {
  constructor(public router: Router,
    private auth: AuthsService,
    private coockie: CookieService,
    private activatedRoute: ActivatedRoute,
    private apiservice: ApiService,) { }
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    // console.log('window.location.pathname', window.location.pathname)
    // console.log('this.cookie', this.coockie.get('wondrlink'))
    if (window.location.pathname == '/' && this.coockie.get('wondrlink') && this.auth.currentUser()) {
      //    console.log('window.location.pathname', window.location.pathname)
      // console.log('this.cookie', this.coockie.get('wondrlink'))
      //       this.auth.getUserByIdInvitationLink(this.coockie.get('wondrlink'))
      this.apiservice.getUserById(this.coockie.get('wondrlink')).subscribe((res: any) => {
        if (res.isSuccess) {
          this.auth.setUser(res.data)
          !res.data?.isPasswordSet ? this.router.navigate(['login-parent'], {
            queryParams: {
              id: this.coockie.get('wondrlink')
            }
          }) : this.router.navigate(['my-wondrfly']);

          // this.auth.getUserByIdInvitationLink(this.coockie.get('wondrlink'))
        }
      })
    }
    else if (!this.auth.currentUser() && this.coockie.get('wondrlink') && window.location.pathname == '/') {
      // this.auth.getUserByIdInvitationLink(this.coockie.get('wondrlink'))
      // this.router.navigate(['login-parent'], {
      //   queryParams: {
      //     id: this.coockie.get('wondrlink')
      //   }
      // })
      this.apiservice.getUserById(this.coockie.get('wondrlink')).subscribe((res: any) => {
        if (res.isSuccess) {
          this.auth.setUser(res.data)
          !res.data?.isPasswordSet ? this.router.navigate(['login-parent'], {
            queryParams: {
              id: this.coockie.get('wondrlink')
            }
          }) : this.router.navigate(['my-wondrfly']);

          // this.auth.getUserByIdInvitationLink(this.coockie.get('wondrlink'))
        }
      })
    }
    else
      if (!this.auth.currentUser()) {
        return true
      } else { this.router.navigate(["my-wondrfly"]); }
  }
}
// @Injectable()
// export class WondrLink implements CanActivate {
//   constructor(public router: Router, private auth: AuthsService, private coockie: CookieService) { }
//   canActivate(): Observable<boolean> | Promise<boolean> | boolean {
//     let id = this.coockie.get('wondrlink')
//     console.log("active G", id)
//     if (!id) {
//       return true
//     } else {
//       this.router.navigate(['login-parent'], {
//         queryParams: {
//           id: id
//         }
//       })
//     }
//   }
// }1
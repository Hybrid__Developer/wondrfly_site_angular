import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models';
import { Child } from '../models/child.model';
import { Category } from '../models/category.model';
import { Program } from '../models/program.model';
import { Tag } from '../models/tag.model';
import { LocalStorageService } from '.';
import { environment } from 'src/environments/environment';
import { SocialUser } from '../models/social.model';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiUrls.reports;
    userResponse: any;
    header = {}
    public profileProgress = new BehaviorSubject<string>('');
    public progress = this.profileProgress.asObservable();
    constructor(private http: HttpClient) {
    }

    //------------------------ otp request --------------------------->

    otpRequest(email): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/otp?email=${email}`).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //----------------------------- otp verify ----------------------------->

    otpVerify(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/otpVerify`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //------------------------------- forgot password ---------------------->

    forgotPassword(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/forgotPassword`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //--------------------------- reset password --------------------------->

    resetPassword(id, model): Observable<User> {
        const subject = new Subject<User>();
        this.http.put(`${this.root}/users/resetPassword/${id}`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }


    //------------------------ otp request verify phone number --------------------------->

    sendOtp(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/twilio/sendOtpSMS`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }



    //---------------------------- upload user Image ------------------------>

    uploadUserImage(id, model): Observable<any> {
        const subject = new Subject<any>();
        this.http.put(`${this.root}/users/uploadProfilePic/${id}`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    //---------------------------------------------- remove user Image ---------------------------------------------->
    removeUserImage(id): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.put(`${this.root}/users/removeProfilePic?id=${id}`, '',).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //---------------------------------------------- remove child Image ---------------------------------------------->
    removeChildImage(id): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.put(`${this.root}/child/removeProfilePic?id=${id}`, '',).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }
    //----------------------------- get pic url ------------------------------->

    getPicUrl(pic) {
        const subject = new Subject<any>();
        this.http.post(`${this.root}/uploads/getPicUrl`, pic).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();

    }


    //----------------------------- upload child Image ----------------------->

    uploadChildImage(model): Observable<any> {
        const subject = new Subject<any>();
        this.http.post(`${this.root}/child/uploadChildPic`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();

    }

    //------------------------- add user --------------------------------->

    addUser(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/register`, data).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    //------------------------- ask to join  user --------------------------------->

    askToJoin(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/invitation/askToJoin`, data,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    // -------------------get users-----------------------

    getUsers(role, no, size): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.get(`${this.root}/users/list?pageNo=${no}&pageSize=${size}&role=${role}`).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    searchUsers(name, role): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.get(`${this.root}/users/search?name=${name}&role=${role}`,).subscribe((responseData: any) => {
            if (responseData.isSuccess) {
                this.userResponse = responseData;
                subject.next(this.userResponse);
            }
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    searchKeywords(key): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/filterkeys/search?name=${key}`,).subscribe((responseData: any) => {
            if (responseData.isSuccess) {
                this.userResponse = responseData;
                subject.next(this.userResponse);
            }
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    searchMultipleKeywords(key): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/freetextSearch/search?text=${key}`,).subscribe((responseData: any) => {
            if (responseData.isSuccess) {
                this.userResponse = responseData;
                subject.next(this.userResponse);
            }
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    addKeywordPopularity(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/filterkeys/clickKeyword/${id}`,).subscribe((responseData: any) => {
            if (responseData.isSuccess) {
                this.userResponse = responseData;
                subject.next(this.userResponse);
            }
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    //------------------------- contactUs --------------------------------->

    contactUs(data): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/users/contactUs`, data).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    //---------------------------- get parent by Id -------------------------->

    getParentById(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/parents/getById/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //------------------------------- update parent -------------------------->

    updateParent(id, data, passwordSetRequest?): Observable<User[]> {
        let url = passwordSetRequest && passwordSetRequest !== undefined ? `/parents/update/${id}${passwordSetRequest}` : `/parents/update/${id}`;
        const subject = new Subject<User[]>();
        this.http.put(`${this.root}${url}`, data,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //------------------------------- analytics parent -------------------------->

    parentAnalytics(key, userId, value): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.put(`${this.root}/parents/createSearchHistory?userId=${userId}&${key}=${value}`, '',).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }



    //---------------------------- active/deactive user ------------------------>

    activeDeactiveUser(id, isActivated): Observable<User> {
        const subject = new Subject<User>();
        this.http.put(`${this.root}/users/activeOrDeactive?id=${id}&isActivated=${isActivated}`, '',).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    // -------------------------- get child by parent Id ------------------------>

    getChildByParentId(id): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.get(`${this.root}/child/byParentId/${id}`,).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- get Active child by parent Id ------------------------>

    getActiveChildByParent(id): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.get(`${this.root}/child/activatedChildByParentId/${id}`,).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ----------------------------- add child ------------------------------->

    addChild(model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.post(`${this.root}/child/add`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    // ----------------------------- add multiple child ------------------------------->

    addMultipleChild(model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.post(`${this.root}/child/addMultiple`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }


    // ----------------------------- invite guardian ------------------------------->

    inviteGuardian(model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/guardians/inviteToJoin`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ----------------------------- add/signup guardian ------------------------------->

    signupGuardian(model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.post(`${this.root}/guardians/add`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ----------------------------- delete guardian ------------------------------->

    deleteChild(id): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        let model: any;
        this.http.put(`${this.root}/child/delete/${id}`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }


    // ----------------------------- delete guardian ------------------------------->

    activeDactiveChild(id, value): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        let model: any;
        this.http.put(`${this.root}/child/activeOrDeactive?id=${id}&isActivated=${value}`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ----------------------------- activedeactive guardian ------------------------------->

    activedeactiveGuardian(id, value): Observable<User[]> {
        const subject = new Subject<User[]>();
        let model: any;
        this.http.put(`${this.root}/guardians/activeOrDeactive?id=${id}&isActivated=${value}`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ----------------------------- update guardian ------------------------------->

    updateGuardian(model): Observable<User[]> {
        const subject = new Subject<User[]>();
        this.http.put(`${this.root}/guardians/update/${model.id}`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ----------------------------- update child ------------------------------->

    updateChild(id, model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.put(`${this.root}/child/update/${id}`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    //-------------------------- get category -------------------------->

    getCategory(): Observable<Category> {
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/categories/list`).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    getAllCategory(): Observable<Category> {
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/categories/getAll`).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ----------------------------- search category -------------------------->

    searchCategory(key): Observable<Category> {
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/categories/search?name=${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ----------------------------- program search -------------------------->

    programSearch(key): Observable<Category> {
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/programs/search?name=${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ----------------------------- program get by lat long or (location) -------------------------->

    programByLatLng(lat, lng): Observable<Category> {
        const subject = new Subject<Category>();
        this.http.get(`${this.root}/programs/nearBy?lat=${lat}&lng=${lng}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ----------------------------- program filter -------------------------->

    programFilter(filter, pageNo, pageSize): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/multiFilter?${filter}&pageNo=${pageNo}&pageSize=${pageSize}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(dataModel);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }


    activityByNameDate(activityName) {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/searchByNameAndDate?programName=${activityName}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }


    // ----------------------------- search tag ------------------------------>

    searchTag(key): Observable<Tag> {
        const subject = new Subject<Tag>();
        this.http.get(`${this.root}/tags/search?name=${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ----------------------------- search tag modified API for child add and update------------------------------>

    searchTagForChildAddUpdate(key): Observable<Tag> {
        const subject = new Subject<Tag>();
        this.http.get(`${this.root}/tags/searchTags?name=${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ----------------------------- get tag -------------------------->

    getTag(): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/tags/list`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- get Tag By Category Id ------------------------->

    getTagByCategoryId(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/tags/byCategoryId?catrgoryIds=${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // --------------------------get Favourite ByParent Id------------------------->

    getFavouriteByParentId(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/favourites/getByParentId?parentId=${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // --------------------------get guardian ByParent Id------------------------->

    getGuardianByParentId(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/guardians/byParentId/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- get user by Id ------------------------->

    getUserById(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/getById/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // -------------------------- getUserByUsername ------------------------->

    getUserByUsername(key): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/providers/getByUsername/${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- search topic ------------------------->

    searchTopic(key): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/searchTopics/getByName/${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ---------------------------delete notification by id---------------------

    deleteNotification(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.delete(`${this.root}/notification/deleteNotification?id=${model._id}&userId=${model.user}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ---------------------------delete notification by id---------------------

    readNotification(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.put(`${this.root}/notification/isRead/${id}`, '').subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // ---------------------------on/off notification by id---------------------

    onOffNotification(id, e): Observable<User> {
        const subject = new Subject<User>();
        this.http.put(`${this.root}/notification/onOff?id=${id}&status=${e}`, '',).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- get Profile Progress ------------------------->

    getProfileProgress(id, role): Observable<User> {
        const subject = new Subject<User>();
        this.http.get(`${this.root}/users/getProfileProgress?id=${id}&role=${role}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            this.profileProgress.next(responseData.data.profileProgress)
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    getPublishedProgram(pageNo, pageSize, programType): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/listPublishOrUnpublish?pageNo=${pageNo}&pageSize=${pageSize}&programType=${programType}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ---------------------------------------------------programs list by providers------------------------------------------------------
    getPublishedProgramByProvider(pageNo, pageSize, programType): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/groupPublishOrUnpublish?pageNo=${pageNo}&pageSize=${pageSize}&programType=${programType}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    //  save provider 
    saveProvider(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/providers/saveProvider`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //  get saved provider 
    getSavedProvidersByParentId(parentId): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/favourites/savedProvidersUserId?parentId=${parentId}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    //  un-save provider 
    unsaveProviders(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.delete(`${this.root}/favourites/unsaveProvider/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);

        });
        return subject.asObservable();
    }

    programPublishUnpublish(id, isPublished) {
        const subject = new Subject<Program[]>();
        this.http.put(`${this.root}/programs/publish?programId=${id}&isPublished=${isPublished}`, '',).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    // -------------------------- get Program by provider ------------------------->

    getProgramByProvider(userId, pageNo, pageSize): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/byProvider?userId=${userId}&pageNo=${pageNo}&pageSize=${pageSize}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }



    // -------------------------- get Program All ------------------------->
    getProgram(pageNo, pageSize): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/list?pageNo=${pageNo}&pageSize=${pageSize}`,).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }



    // -------------------------- get Program by id ------------------------->

    getProgramById(id): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/getById/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- add Fav Program ------------------------->

    addFavProgram(model): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.post(`${this.root}/favourites/add`, model,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- delete Fav Program ------------------------->

    deleteFavProgram(id): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.delete(`${this.root}/favourites/delete/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }


    // -------------------------------- get view ------------------------------>

    getView(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/getViewsCount?userId=${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- search program ------------------------->

    searchProgram(key): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/search?name=${key}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // -------------------------- add action ------------------------->

    addAction(data): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.post(`${this.root}/programs/addProgramAction`, data,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }



    //------------------------ get badge list --------------------------->

    badgeList(): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.get(`${this.root}/badges/list`,).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    getOldChat(roomId, pageNo, pageSize): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.get(`${this.root}/conversations/getOldChat?room_id=${roomId}&pageNo=${pageNo}&pageSize=${pageSize}`,).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    //-------------------------------- Signup with Facebook --------------------------------->

    signupWithFb(model): Observable<SocialUser[]> {
        const subject = new Subject<SocialUser[]>();
        this.http.post(`${this.root}/users/facebook/login`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }



    //-------------------------------- Signup with Google --------------------------------->

    signupWithGoogle(model): Observable<SocialUser[]> {
        const subject = new Subject<SocialUser[]>();
        this.http.post(`${this.root}/users/loginWithGoogle`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    //--------------------------------------clear all notification --------------------------------------------------------------->
    clearAllNotifications(id): Observable<User> {
        const subject = new Subject<User>();
        this.http.delete(`${this.root}/notification/deleteAll?id=${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    sendFeedback(model): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.post(`${this.root}/feedback/create`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    //-------------------------------------- Get users rating by id --------------------------------------------------------------->
    getUserRating(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/providers/getRatingByUser/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }


    //-------------------------------------- top rated programs --------------------------------------------------------------->
    getTopRated(): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/topRating`).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData.data);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    // ------------------------------------------subCategory fillter--------------------------------------------

    programBySubCategoryIds(filter, pageNo, pageSize): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/subCategoryFilter?${filter}&pageNo=${pageNo}&pageSize=${pageSize}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            // this.toasty.error(dataModel.error);
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    //-------------------------------------- Get users rating by id --------------------------------------------------------------->
    getSuggestedCategory(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/suggestion/bySubcategoryId/${id}`,).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    // this.toasty.error(dataModel.error);
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }

            subject.next(responseData.data);

        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }

    InviteAsktojoin(model): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.post(`${this.root}/invitation/inviteToJoin`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    guardianAsktojoin(model): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.post(`${this.root}/guardians/askToJoin`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            const dataModel = error;
            subject.next(dataModel.error);
        });
        return subject.asObservable();
    }
    feedbackSurveyList(): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.get(`${this.root}/justfeedback/list`,).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getInvitedUsersByParent(id): Observable<any[]> {
        const subject = new Subject<any[]>();
        this.http.get(`${this.root}/invitation/listByParentId/${id}`,).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    subscribeToMailchimpNewsletter(model): Observable<any> {
        const subject = new Subject<any>();
        this.http.post(`${this.root}/marketMailchimp/addSubscriber`, model).subscribe((responseData: any) => {
            if (responseData.statusCode !== 200) {
                throw new Error('This request has failed ' + responseData.status);
            }
            const dataModel = responseData;
            if (!dataModel.isSuccess) {
                if (responseData.status === 200) {
                    throw new Error(dataModel.code || dataModel.message || 'failed');
                } else {
                    throw new Error(responseData.status + '');
                }
            }
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    // -------------------get users-----------------------

    childTagProgramCount(tagId, childAge): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/childTagProgramCount?tagId=${tagId}&maxAge=${childAge}`,).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    childrenWithFiltredActivity(childIds): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/child/interestPrograms?childIds=${childIds}`,).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    // =============================get users=================================
    getMetaServiceByPageName(pageName): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/metaservice/getbyPagename/${pageName}`,).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    addUserEmail(id, model, token): Observable<any> {
        let header;
        if (token != '') {
            header = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'x-access-token': token
                })
            }
        }
        const subject = new Subject<any>();
        this.http.post(`${this.root}/users/emailUpdate/${id}`, model, header).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getProgramsGroup(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/programs/groupBySametypeAndage/${id}`,).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    // get suggestion sub-Categories by parent id 
    getSuggestion(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/suggestion/byParentId/${id}`).subscribe((responseData: any) => {
            if (responseData.isSuccess) {
                this.userResponse = responseData;
                subject.next(this.userResponse);
            }
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getProgramByType(pageNo, pageSize, type): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/getByType?pageNo=${pageNo}&pageSize=${pageSize}&type=${type}`).subscribe((responseData: any) => {
            if (responseData.isSuccess) {
                this.userResponse = responseData;
                subject.next(this.userResponse);
            }
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getProgramsActivity(id): Observable<any> {
        const subject = new Subject<any>();
        this.http.get(`${this.root}/activity/ByProgramId/${id}`).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    getIPAddress() {
        return this.http.get("https://api.ipify.org/?format=json");
    }
}

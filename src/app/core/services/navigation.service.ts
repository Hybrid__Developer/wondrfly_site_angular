import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {
  currentRoute = new BehaviorSubject<string>('');
  nonParameterRoute = new BehaviorSubject<string>('');
  // navigation component has subscribed to this Observable
  nonParameterRoute$ = this.nonParameterRoute.asObservable();
  route$ = this.currentRoute.asObservable();
  constructor(private router: Router) {
    this.getRoute();
  }

  public get routeValue(): string {
    return this.currentRoute.value;
  }

  getRoute() {
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((routeChange: NavigationEnd) => {
        let Route = this.router.url;
        let nonParameterRoute = this.router.url.split('?')[0];
        this.nonParameterRoute.next(nonParameterRoute)
        this.currentRoute.next(Route)
      });
  }
}

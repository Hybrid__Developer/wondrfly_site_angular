import { I } from '@angular/cdk/keycodes';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models';
import { AuthsService } from './auths.service';
import { LocalStorageService } from './local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class BlogApiService {
  root = environment.blogsUrl;
  wfUser: User;

  constructor(
    private auths: AuthsService,
    private store: LocalStorageService) {}

  setStrapiUser(user){
    this.store.setObject("strapiData", user);
    this.store.setItem("jwt", user.jwt);
  }

  // ------------------------------------------------ blogs  -------------------------------------------
  getBlog(url: string): Observable<any> {
    const subject = new Subject<any>();
    axios.get(`${this.root}/blogs${url}`).then(response => {
      subject.next(response.data);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }
  addBlogView(url: string): Observable<any> {
    const subject = new Subject<any>();
    axios.patch(`${this.root}/blogs${url}`).then(response => {
      subject.next(response.data);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }




  // ------------------------------------------------ Category  -------------------------------------------
  getCategory(url: string): Observable<any> {
    const subject = new Subject<any>();
    axios.get(`${this.root}/categories${url}`).then(response => {
      subject.next(response.data);
    }, (error) => {
      subject.next(error.error);
    });
    return subject.asObservable();
  }

  // --------------------------------------login strapi---------------------------------------------------
  strapiLogin() {
    this.wfUser = this.auths.userValue;
    if(this.wfUser){
    axios.post(`${this.root}/auth/local`,{
            password: "strapipassword",
            identifier: this.wfUser.email,
          })
      .then((response) => {
        if (response.status === 200) {
          this.setStrapiUser(response.data);
        }
      })
      .catch((error) => {
        if (error.response.data.statusCode === 400) {
          this.strapiSignup();
        } 
      });
    }
  }

  // --------------------------------------if user is not registerd in strapi------------------------------------
  strapiSignup() {
    axios
      .post(`${this.root}/auth/local/register`,{
              username: this.wfUser.firstName,
              email: this.wfUser.email,
              password: "strapipassword",
            })
      .then((response) => {
        if (response.status === 200) {
          this.setStrapiUser(response.data);
        }
      })
    }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { User } from '../models';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

import { LocalStorageService } from '.';
import { Role } from '../models/role.model';
import { ToastrService } from 'ngx-toastr';
import { ApiService } from './api.service.service';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { routes } from 'src/app/pages/parent/parent.routing';

@Injectable({
  providedIn: 'root'
})
export class AuthsService {
  root = environment.apiUrls.reports;
  private _user: User;
  private userSubject: BehaviorSubject<User>;
  public userData: Observable<User>;

  constructor(
    private http: HttpClient,
    private toastr: ToastrService,
    private apiservice: ApiService,
    private store: LocalStorageService,
    private myRoute: Router,
  ) {
    this.userSubject = new BehaviorSubject<User>(this.store.getObject('CurrentUserWondrfly'));
    this.userData = this.userSubject.asObservable();
  }
  setUser(user: User) {
    if (user) {
      user.id = user.id ? user.id : user._id
      this.store.setObject('CurrentUserWondrfly', user);
      this.store.setItem('currentUserWondrflyToken', user.token);
      // this.userSubject.next(user);
    } else {
      this.clearStore();
    }
    this._user = user;
    this.userSubject.next(user);
  }

  currentUser(): User {
    this._user = this.store.getObject('CurrentUserWondrfly') as User;
    return this._user
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  getUserbyId(data: any) {
    let updatedUser: any;
    this.apiservice.getUserById(this.userValue.id).subscribe((res: any) => {
      if (res.isSuccess) {
        updatedUser = res.data;
        this.store.setObject('CurrentUserWondrfly', updatedUser);
        this.userSubject.next(updatedUser);
        data.refreshProgress ? this.getProfileProgress() : '';
      }
    })
  }
  getUserByIdInvitationLink(id: any) {
    this.apiservice.getUserById(id).subscribe((res: any) => {
      if (res.isSuccess) {
        // res.data?.onBoardingCount?.count < 5  && !res.data?.isPasswordSet ? this.setUser(res.data) : this.logout()
     !res.data?.isPasswordSet ? this.setUser(res.data) :this.myRoute.navigate(['my-wondrfly']),this.setUser(res.data);

      }
    })
  }
  getProfileProgress() {
    let id = this.userValue.id ? this.userValue.id : this.userValue._id;
    this.apiservice.getProfileProgress(id, this.userValue.role)
      .subscribe((res: any) => {
        this.apiservice.profileProgress.next(res.profileProgress);
      });
  }

  login(model): Observable<User[]> {
    const subject = new Subject<User[]>();
    this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {
      if (responseData.statusCode !== 200) {
        throw new Error('This request has failed ' + responseData.status);
      }
      const dataModel = responseData
      if (!dataModel.isSuccess) {
        if (responseData.status === 200) {
          this.toastr.error(dataModel.error);
          throw new Error(dataModel.code || dataModel.message || 'failed');
        } else {
          throw new Error(responseData.status + '');
        }
      }
      subject.next(dataModel);
    },
      (error) => {
        subject.next(error.error);
      });
    return subject.asObservable();
  }
  isAuthorized() {
    return !!this._user;
  }

  hasRole(role: Role) {
    return this.isAuthorized() && this._user.role === role;
  }

  logout() {
    this.myRoute.navigate(['']);
    this.clearStore();
    this.setUser(null);
    this.userSubject.next(null);
  }

  clearStore() {
    this.store.removeItem('CurrentUserWondrfly');
    this.store.removeItem('currentUserWondrflyToken');
    this.store.removeItem('strapiData');
    this.store.removeItem('jwt');
  }

}

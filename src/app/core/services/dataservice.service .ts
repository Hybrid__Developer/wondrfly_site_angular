import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private data: any={};
  private location: any;
  private scrollToActivities:any;
  @Output() changeInputValue: EventEmitter<any> = new EventEmitter();
  setOption(option) {
    this.data = option;
  }
  getOption() {
    return this.data;
  }
  setLocation(option) {
    this.location = option;
  }
  getLocation() {
    return this.location;
  }
  setScrollToActivities(option) {
    this.scrollToActivities = option;
  }
  getScrollToActivities(){
    return this.scrollToActivities;
  }
  changeInput(data) {
      this.changeInputValue.emit(data);
   }
  getEmittedValue() {
    return this.changeInputValue;
  }

  constructor() { }
}
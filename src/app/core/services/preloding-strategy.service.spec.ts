import { TestBed } from '@angular/core/testing';

import { PrelodingStrategyService } from './preloding-strategy.service';

describe('PrelodingStrategyService', () => {
  let service: PrelodingStrategyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrelodingStrategyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

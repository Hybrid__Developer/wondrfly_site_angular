import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { SharedModule } from './shared/shared.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { LoaderFileComponent } from './shared/shared-components/loader-file/loader-file.component';
import { MapTheme } from './common/map-theme';

export function playerFactory() {
  return player;
}

const components = [LoaderFileComponent]

const thirdPartyModules = [
  SharedModule,
  LazyLoadImageModule,
  // ImageCropModule,
];
@NgModule({
  imports: [
    CommonModule,
    LottieModule.forRoot({ player: playerFactory }),
    ...thirdPartyModules,
  ],
  declarations: [...components],
  exports: [...thirdPartyModules, ...components],
  providers: [MapTheme, DatePipe]
})
export class CoreModule { }

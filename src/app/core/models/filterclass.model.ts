export class FilterClass {
    typeClass: boolean;
    dateClass: boolean;
    timeClass: boolean;
    dayClass: boolean;
    durationClass: boolean;
    categoryClass: boolean;
    subCategoryClass: boolean;
    ageClass: boolean;
    privateGroupClass: boolean;
    inpersonOnlineClass: boolean;
    childCareClass: boolean;
    priceClass: boolean;
    formatClass: boolean;
    freeClass: boolean;
    ratingClass: boolean;
    highToLowClass: boolean;
    lowToHighClass: boolean;
    nearestDateClass: boolean;
    kidActiveClass: boolean;
}

export class FilterIcons {
    intrestIcon: boolean = true;
    ratingIcon: boolean;
    typeIcon: boolean;
    dateIcon: boolean;
    timeIcon: boolean;
    dayIcon: boolean;
    durationIcon: boolean;
    categoryIcon: boolean;
    subCategoryIcon: boolean;
    ageIcon: boolean;
    privateGroupIcon: boolean;
    inpersonOnlineIcon: boolean;
    childCareIcon: boolean;
    priceIcon: boolean;
    formatIcon: boolean;
    freeIcon: boolean;
    highToLowIcon: boolean;
    lowToHighIcon: boolean;
    nearestDateIcon: boolean;
}

export class FilterValue {
    typeValue: boolean;
    dateValue: boolean;
    ratingValue: boolean;
    timeValue: boolean;
    dayValue: boolean;
    durationValue: boolean;
    categoryValue: boolean;
    subCategoryValue: boolean;
    ageValue: boolean;
    yearValue: boolean;
    monthValue: boolean;
    privateGroupValue: boolean;
    inpersonOnlineValue: boolean;
    childCareValue: boolean;
    priceValue: boolean;
    formatValue: boolean;
    freeValue: boolean;
    highToLowValue: boolean;
    lowToHighValue: boolean;
    nearestDateValue: boolean;
    topRated: boolean;
    activeKid: string;
}
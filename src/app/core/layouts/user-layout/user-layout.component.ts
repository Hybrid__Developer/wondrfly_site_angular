import { Component, OnInit} from '@angular/core';
import {
  Router,
  RouteConfigLoadStart,
  RouteConfigLoadEnd,
  ResolveStart,
  ResolveEnd
} from '@angular/router';
import { Subscription } from "rxjs";
import { User } from '../../models';
import { AuthsService } from '../../services/auths.service';
import { NavigationService } from '../../services/navigation.service';

@Component({
  selector: 'app-user-layout',
  templateUrl: './user-layout.template.html',
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserLayoutComponent implements OnInit {
  public isModuleLoading: Boolean = false;
  private moduleLoaderSub: Subscription;
  private layoutConfSub: Subscription;
  private routerEventSub: Subscription;

  public scrollConfig = {}
  public layoutConf: any = {};
  public adminContainerClasses: any = {};
  currentUrl: any;
  user: User;
  subscribeUser: Subscription;
  subscribeProgress: Subscription;
  profileProgress: any;

  constructor(
    private auths: AuthsService,
    private router: Router,
    private navigation : NavigationService
  ) {
    this.user = auths.userValue;
    this.subscribeUser = this.auths.userData.subscribe(res => {
      this.user = res;
    });
    // Close sidenav after route change in mobile
    this.routerEventSub = navigation.route$.subscribe(res =>{
      this.currentUrl = res;
    })
  }
  ngOnInit() {
    if(this.user){
    this.refreashUser();
    }
    this.moduleLoaderSub = this.router.events.subscribe(event => {
      if (event instanceof RouteConfigLoadStart || event instanceof ResolveStart) {
        this.isModuleLoading = true;
      }
      if (event instanceof RouteConfigLoadEnd || event instanceof ResolveEnd) {
        this.isModuleLoading = false;
      }
    });
  }

  refreashUser(){
    this.auths.getUserbyId({refreshProgress: false})
  }

  scrollToTop() {
    if (document) {
      setTimeout(() => {
        let element;
        if (this.layoutConf.topbarFixed) {
          element = <HTMLElement>document.querySelector('#rightside-content-hold');
        } else {
          element = <HTMLElement>document.querySelector('#main-content-wrap');
        }
        // element.scrollTop = 0;
      })
    }
  }
  ngOnDestroy() {
    if (this.moduleLoaderSub) {
      this.moduleLoaderSub.unsubscribe();
    }
    if (this.layoutConfSub) {
      this.layoutConfSub.unsubscribe();
    }
    if (this.routerEventSub) {
      this.routerEventSub.unsubscribe();
    }
    this.subscribeUser.unsubscribe();
  }

}
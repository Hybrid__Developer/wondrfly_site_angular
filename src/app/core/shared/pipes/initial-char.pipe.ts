import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initialChar'
})
export class InitialCharPipe implements PipeTransform {

  transform(name: string) {
    var arr = name.split(' ');
    arr = arr.map(function (el) {
      return el.trim();
    });
    arr = arr.filter(a => a)
    return arr.length == 1 ? name?.charAt(0).toUpperCase() + name?.charAt(1).toUpperCase() : this.initials(arr)
  }
  initials(arr) {
    let initial = ''
    for (let i in arr) {
      if (+i >= 2) { break; }
      initial += arr[i]?.charAt(0)
    }
    return initial.toUpperCase()
  }

}

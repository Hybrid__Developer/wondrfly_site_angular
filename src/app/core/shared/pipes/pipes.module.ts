import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitialCharPipe } from './initial-char.pipe';
import { TimeConvertPipe } from './time-convert.pipe';
import { ViewStreetPipe } from './view-street.pipe';
import { DaysfilterPipe } from './daysfilter.pipe';
import { AgeRangePipe } from './age-range.pipe';

const customPipes = [InitialCharPipe,
  TimeConvertPipe,
  ViewStreetPipe,
  DaysfilterPipe,
  AgeRangePipe
]

@NgModule({
  declarations: [
    ...customPipes,
  ],
  imports: [
    CommonModule
  ],
  exports: [...customPipes],
  providers: [...customPipes]
})
export class pipesModule { }

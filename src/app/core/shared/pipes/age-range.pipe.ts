import { I } from '@angular/cdk/keycodes';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ageRange'
})
export class AgeRangePipe implements PipeTransform {

  transform(ageArray: any): unknown {
    if(ageArray?.year?.length > 0 && ageArray?.month?.length == 0){
      ageArray?.year?.sort(function (a, b) { return a - b })
      return ageArray?.year?.length > 1 ? ageArray?.year[0] +" yrs "+ "- " + ageArray?.year[ageArray?.year?.length - 1] + " yrs" : ageArray?.year?.length == 1 ? ageArray?.year[0] + " yrs" : 'Age is Not Mentioned';
    }
    else if(ageArray?.year?.length == 0 && ageArray?.month?.length > 0){
      ageArray?.month?.sort(function (a, b) { return a - b })
      return ageArray?.month?.length > 1 ? ageArray?.month[0] +" months"+ "-" + ageArray?.month[ageArray?.month?.length - 1] + " months" : ageArray?.month?.length == 1 ? ageArray?.month[0] + " months" : 'Age is Not Mentioned';
    }
    else if(ageArray?.year?.length > 0 && ageArray?.month?.length > 0){
      ageArray?.year?.sort(function (a, b) { return a - b })
      ageArray?.month?.sort(function (a, b) { return a - b })
      return ageArray?.month[0] +" months"+ "-" + ageArray?.year[ageArray?.year?.length - 1] + "yrs" ;
    }
    else{
      return 'Age is Not Mentioned'
    }
   
  }

}


// <p *ngIf="program?.ageGroup?.year?.length >1">{{program?.ageGroup?.year[0]}}
//                                   -{{program?.ageGroup?.year[program?.ageGroup?.year?.length - 1]}} yrs</p>
//                                 <p *ngIf="program?.ageGroup?.year?.length==1">{{program?.ageGroup?.year[0]}} yrs</p>
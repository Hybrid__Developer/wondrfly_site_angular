import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'viewStreet'
})
export class ViewStreetPipe implements PipeTransform {

  transform(data): unknown {
    let name =  data.indexOf(", ")>-1?data?.substring(0, data.indexOf(", ")):data
    name = name=="New Jersey NJ"?name:name.replace('New Jersey NJ', '')
    name = name=="New Jersey"?name:name.replace('New Jersey', '')
    name = name=="Jersey City"?name:name.replace('Jersey City', '')
    name = name=="Jersey City NJ"?name:name.replace('Jersey City NJ', '')
    return name;
  }

}

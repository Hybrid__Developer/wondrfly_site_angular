import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appScrollHorizontal]'
})
export class ScrollHorizontalDirective {
  mouseDown = false;

  startX: any;

  scrollLeft: any;
  constructor(private el: ElementRef) {
  }

  @HostListener("wheel", ["$event"])
  public onScroll(event: WheelEvent) {
    event.preventDefault();
    this.el.nativeElement.scrollLeft += event.deltaY;
  }
}
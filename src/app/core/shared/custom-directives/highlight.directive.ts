import { AfterViewInit, Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[highlight]'
})
export class HighlightDirective implements AfterViewInit {
  constructor(private renderer: Renderer2, private el: ElementRef) { }

  changeStyle() {
    let myCurrentContent: any = this.el.nativeElement.innerText;
    switch (myCurrentContent) {
      case 'Camps': {
        this.renderer.addClass(this.el.nativeElement, 'camp-btn-bt');
        break;
      }
      case 'Drop-ins':
      case 'Drops-in': {
        this.renderer.addClass(this.el.nativeElement, 'drop-in-btn-bt');
        break;
      }
      case 'Semesters': {
        this.renderer.addClass(this.el.nativeElement, 'semester-btn-bt');
        break;
      }
    }
  }

  ngAfterViewInit(): void {
    this.changeStyle();
  }
}

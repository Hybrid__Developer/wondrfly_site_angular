import { Component, ElementRef, HostListener, NgZone, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MapsAPILoader } from "@agm/core";
import * as moment from "moment";
import { FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { User } from "src/app/core/models";
import { LocalStorageService } from "src/app/core/services";
import { ApiService } from "src/app/core/services/api.service.service";
import { AuthsService } from "src/app/core/services/auths.service";
import { DataService } from "src/app/core/services/dataservice.service ";
import { NavigationService } from "src/app/core/services/navigation.service";

@Component({
  selector: "app-header",
  templateUrl: './header.component.html',
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit, OnDestroy {
  isLogin = false;
  user = new User;
  refresh: Subscription;
  routeName: string;
  isToggle: boolean;
  profileClass: string = "active";
  programClass: string = "";
  settingClass: string = "";
  insightClass: string = "";
  helpClass: string = "";
  forumClass: string = "";
  chatClass: string = "";
  lat: string = "40.719074";
  lng: string = "-74.050552";
  filterData: any = {
    subcatId: '',
    categoryId: '',
    activityName: '',
    searchedCategoryKey: '',
    lat: '',
    lng: '',
  };
  locationData: any = {
    lat: '',
    lng: '',
  }

  searchTerm = new FormControl();
  zoom = 14;
  allData: any = [];

  initialUrl: any;
  feedbackData: any = {
    id: "",
    feedback: "",
  };
  logoPosition = false;
  searchBar = false;
  notification: any;
  profileProgressResponse: any;
  progressBarValue: any;
  profileProgress: any;
  message: string = "Please Login First!";
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  categoriesBySearch: any;
  providersBySearch: any;
  address: string;
  private geoCoder;
  todayNotifications = [];
  earlierNotifications = [];
  newNotifications: any;
  subscription: Subscription
  @ViewChild('search', { static: false }) searchElementRef: ElementRef; gitBoxImage = 'assets/gift-box.svg';
  categoryData: any;
  filterArray: any = []
  profileProgressPopup: boolean
  subscribeUser: Subscription;
  subscribeProgress: Subscription;
  routerEventSub: Subscription;
  isDistance: boolean;
  distance: number = 16093.44;
  isBird: boolean = false
  isDriving: boolean = false
  isBiking: boolean = false
  isWalking: boolean = false
  isWith: boolean = false
  locationSearched = '';
  // @HostListener('document:click', ['$event']) clickedOutside($event) {
  // here you can hide your review popup
  // this.isDistance = false;
  // }
  savedProvider: any;
  selectedDistance: string = "Distance";
  filterObj: any;
  constructor(
    private dataService: DataService,
    private router: Router,
    private auth: AuthsService,
    private apiservice: ApiService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public dataservice: DataService,
    public store: LocalStorageService,
    private navigation: NavigationService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.routerEventSub = navigation.nonParameterRoute$.subscribe(res => {
      this.routeName = res;
      if (this.routeName === '' || this.routeName === '/invite') { this.searchBar = true } else { this.searchBar = false };
      if (this.routeName === '/search') { this.logoPosition = true } else { this.logoPosition = false; }
    })
    this.user = auth.userValue;
    this.subscribeUser = this.auth.userData.subscribe(res => {
      this.user = res;
      this.getNotificationSetting(this.user);
    })
    this.subscribeProgress = apiservice.progress.subscribe(res => {
      this.profileProgress = res;
      this.progressCircle(this.profileProgress);
    })
    if (this.routeName === "invite") {
      this.gitBoxImage = 'assets/invite-open.svg';
    }
    if (this.user.role === "provider" || this.user.role === "parent") {
      if (this.user.role === "provider") {
        this.isLogin = true;
      } else if (this.user.role === "parent") {
        this.isLogin = false;
      }
    } else {
      auth.logout();
    }
  }
  scrollToTop() {
    throw new Error("Method not implemented.");
  }
  savedProviders() {
    this.apiservice.getSavedProvidersByParentId(this.user.id).subscribe((res: any) => {
      this.savedProvider = res?.data?.length;
    });
  }
  logo() {
    this.router
      .navigateByUrl("/", { skipLocationChange: true })
      .then(() => this.router.navigate(["my-wondrfly"]));
  }
  addProgram() {
    this.router.navigate(["/provider/program/add"]);
  }
  profile() {
    this.store.removeItem('activeList')
    if (this.user.role === "parent") {
      this.router.navigate(["parent/profile", this.user.id]);
      // if (this.routeName === "parent/profile", this.user.id) {
      //   this.router
      //     .navigateByUrl("/", { skipLocationChange: true })
      //     .then(() => this.router.navigate(["parent/profile", this.user.id]));
      // }
    } else if (this.user.role === "provider") {
      this.router.navigate(["provider/profile", this.user.id]);
    } else {
      this.router.navigate(["login"]);
    }
  }

  savedList() {
    if (this.user.role === "parent") {
      this.store.setItem("activeList", "savedList");
      this.router.navigate(["parent/profile", this.user.id]);
      if (this.routeName === "parent/profile", this.user.id) {
        this.router
          .navigateByUrl("/", { skipLocationChange: true })
          .then(() => this.router.navigate(["parent/profile", this.user.id]));
      }
    }
  }
  getProfileProgress() {
    this.apiservice
      .getProfileProgress(this.user.id, this.user.role)
      .subscribe((res: any) => {
        this.profileProgress = res.profileProgress;
        this.progressCircle(this.profileProgress);
      });
  }

  progressCircle(progress) {
    $("#progress").attr("data-percentage", progress);
    if (this.routeName === `/parent/profile/${this.user.id}`) {
      this.profileProgressPopup = false
    } else {
      this.profileProgressPopup = true
    }
  }


  // getUserById(id?) {
  //   this.apiservice.getUserById(this.user.id).subscribe((res: any) => {
  //     this.user = res.data;
  //     this.user.notices.notifications.reverse();
  //     let notifications = []
  //     notifications = this.user.notices.notifications.filter(notification => notification.isRead == false)
  //     this.newNotifications = notifications.length

  //     this.todayNotifications = this.user.notices.notifications.filter(obj => moment().isSame(obj.createdOn, 'day'));
  //     this.earlierNotifications = this.user.notices.notifications.filter(obj => !moment().isSame(obj.createdOn, 'day'));
  //     this.store.setObject('CurrentUserWondrfly', this.user);
  //     if (this.user.notificationsOnOff === true) {
  //       this.isToggle = true;
  //     }
  //   });
  // }

  getNotificationSetting(user: User) {
    let notifications = [];
    let notificatinData = user?.notices;
    notificatinData?.notifications?.reverse();
    notifications = notificatinData?.notifications?.filter(notification => notification.isRead == false)
    this.newNotifications = notifications?.length;
    this.todayNotifications = notificatinData?.notifications?.filter(obj => moment().isSame(obj.createdOn, 'day'));
    this.earlierNotifications = notificatinData?.notifications?.filter(obj => !moment().isSame(obj.createdOn, 'day'));
    if (user?.notificationsOnOff === true) {
      this.isToggle = true;
    }
  }

  clearAll() {
    this.apiservice
      .clearAllNotifications(this.user.id)
      .subscribe((res: any) => {
        this.notification = res;
        this.auth.getUserbyId({ refreshProgress: false });
      });
  }
  deleteNotification(data, indx) {
    this.apiservice.deleteNotification(data).subscribe((res: any) => {
      if (res.isSuccess) {
        this.auth.getUserbyId({ refreshProgress: false });
      }
    });
  }
  readNotification(notification) {
    if (!notification.isRead) {
      this.apiservice.readNotification(notification._id).subscribe((res: any) => {
        if (res.isSuccess) {
          this.auth.getUserbyId({ refreshProgress: false });
        }
      });
    }
  }
  notificationView(data) {
    switch (data.title) {
      case 'update Profile': case 'About Profile': {
        window.document.getElementById("close_notification_modal").click();
        this.store.removeItem('activeList')
        if (this.user.role === "parent") {
          this.router.navigate(["parent/profile", this.user.id]);
          if (this.routeName === "parent/profile", this.user.id) {
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["parent/profile", this.user.id]));
          }
        } else if (this.user.role === "provider") {
          this.router.navigate(["provider/profile", this.user.id]);
        } else {
          this.router.navigate(["login"]);
        }
        break;
      }
      case 'Add child': {
        window.document.getElementById("close_notification_modal").click();
        if (this.user.role === "parent") {
          this.store.setItem("activeList", "kidList");
          this.router.navigate(["parent/profile", this.user.id])
          if (this.routeName === "parent/profile", this.user.id) {
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["parent/profile", this.user.id]));
          }
        }
        break;
      }
      case 'Save Program': case 'unSave Program': {
        window.document.getElementById("close_notification_modal").click();
        if (this.user.role === "parent") {
          this.store.setItem("activeList", "savedList");
          this.router.navigate(["parent/profile", this.user.id])
          if (this.routeName === "parent/profile", this.user.id) {
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["parent/profile", this.user.id]));
          }
        }
        break;
      }
    }
  }
  onOffNotification(id, e) {
    this.apiservice.onOffNotification(id, e).subscribe((res: any) => {
      this.auth.getUserbyId({ refreshProgress: false });
    });
  }
  onTab(e, value) {
    this.searchTerm.setValue(value)
  }
  ngOnInit() {
    // this.getAddress()
    this.getProfileProgress();
    this.savedProviders()
    this.searchTerm.valueChanges.subscribe((value) => {
      if (value) { this.searchSubCategory(value) } else {
        this.allData = [];
      }
    })
    this.activatedRoute.queryParams
      .subscribe((params: any) => {
        if (params.filter) {
          this.filterObj = JSON.parse('{"' + params.filter.replace(/&/g, '","').replace(/=/g, '":"') + '"}', function (key, value) { return key === "" ? value : decodeURIComponent(value) })
          if (this.filterObj.hasOwnProperty('lat') && this.filterObj.hasOwnProperty('lng') || this.filterObj.hasOwnProperty('nearBy')) {
            switch (this.filterObj.nearBy) {
              case "16093.44":
                this.selectedDistance = "Bird’s - eye view";
                this.isBird = true;
                break;
              case "8046.72":
                this.selectedDistance = "Driving (5 mi.)";
                this.isDriving = true;
                break;
              case "3218.688":
                this.selectedDistance = "Biking (2 mi.)";
                this.isBiking = true;
                break;
              case "1609.344":
                this.selectedDistance = "Walking (1 mi.)";
                this.isWalking = true;
                break;
              case "500":
                this.selectedDistance = "Within 4 blocks";
                this.isWith = true;
                break;
            }
          }
        } else {
          this.selectedDistance = "Distance";
          this.isWith = false;
          this.isBird = false;
          this.isBiking = false;
          this.isDriving = false;
          this.isWalking = false;
          this.locationSearched = ""
        }
      })

    this.subscription = this.dataService.getEmittedValue()
      .subscribe(item => this.searchTerm.setValue(item));
    // this.getUserById();
    if ((this.routeName === "/profile", this.user.id)) {
      this.profileClass = "active";
    }
    if (this.routeName === "/program/list") {
      this.programClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "/program/detail") {
      this.programClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "/program/add") {
      this.programClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "help") {
      this.helpClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "/program/home") {
      this.insightClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "/program/setting") {
      this.settingClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "/forum/forum-type") {
      this.forumClass = "active";
      this.profileClass = "";
    }
    if (this.routeName === "/chat") {
      this.chatClass = "active";
      this.profileClass = "";
    }

    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      const center = { lat: 40.71, lng: -74.05 };
      let filter
      // Create a bounding box with sides ~30km away from the center point
      const defaultBounds = {
        north: center.lat + 0.2,
        south: center.lat - 0.2,
        east: center.lng + 0.2,
        west: center.lng - 0.2,
      };
      var options = {
        bounds: defaultBounds,
        types: ['geocode'],
        // componentRestrictions: {country: "us"},
        strictBounds: true,
      };
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef?.nativeElement, options);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude
          this.lat = String(place.geometry.location.lat());
          this.lng = String(place.geometry.location.lng());
          this.locationSearched = place.formatted_address;
          filter = `lat=${this.lat}&lng=${this.lng}&nearBy=${this.distance}`
          this.router.navigate(['/search'], {
            queryParams: {
              filter: filter
            }
          })

        });
      });
    });
  }

  logout() {
    this.auth.logout();
  }
  goToInviteList() {
    this.store.setItem('sendInvite', '1')
    this.router.navigate(['/parent/profile', this.user.id]);
  }
  submitFeedback() {
    this.feedbackData.id = this.user.id;
    this.apiservice.sendFeedback(this.feedbackData).subscribe((res: any) => {
      if (res.isSuccess === true) {
        this.auth.logout();
      }
    });
  }
  cancelFeedback() {
    this.auth.logout();
  }
  providerSearch(key) {
    this.apiservice.searchUsers(key, "provider").subscribe((res: any) => {
      if (res.data) {
        this.providersBySearch = res.data;
      }
      else {
        this.providersBySearch = []
      }
    });
  }
  searchByLocation() {
    this.filterData.searchedCategoryKey = this.filterData.activityName
    this.filterData.activityName = ''
    this.filterData.categoryId = ''
    this.filterData.subcatId = ''
    this.filterData.lat = this.lat
    this.filterData.lng = this.lng
    this.dataservice.setLocation(this.filterData)
    this.router.navigate(['/search']);
    if (this.routeName === "/search") {
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(["search"]));
    }
  }
  searchSubCategory(key) {
    let groupDataAll: any = [
      { label: 'Keywords', data: [] },
      // { label: 'Provider', data: [] },
    ]
    if (!key) {
      this.allData = [];
    } else {
      this.apiservice.searchKeywords(key).subscribe((res: any) => {
        this.categoriesBySearch = res.data;
        res.data.map(keyword => { keyword.name = keyword.keywordName })
        groupDataAll[0].data = this.categoriesBySearch;
        this.allData = groupDataAll
      });
      // this.apiservice.searchUsers(key, "provider").subscribe((res: any) => {
      //     this.providersBySearch = res.data;
      //     this.providersBySearch = this.providersBySearch.filter(e => e.isActivated);
      //     var i;
      //     for (i = 0; i < this.providersBySearch.length; i++) {
      //       this.providersBySearch[i].name = this.providersBySearch[i]['firstName'];
      //       groupDataAll[1].data = this.providersBySearch;
      //   //     this.allData = groupDataAll
      //   // console.log(this.allData)

      //     }
      // });
    }
  }

  searchByCategory(id) {
    this.filterData.activityName = ''
    this.filterData.subcatId = ''
    this.filterData.categoryId = id
    this.filterData.lat = ''
    this.filterData.lng = ''
    this.dataservice.setOption(this.filterData)
    this.router.navigate(["/search"]);
    if (this.routeName === "/search") {
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(["search"]));
    }
  }

  goToProviderProfile(provider) {
    provider.firstName = provider.firstName.toLowerCase();
    provider.firstName = provider.firstName.replace(/ /g, "-");
    provider.firstName = provider.firstName.replace(/\?/g, "-");
    this.router.navigate(["/program/provider", provider.firstName, provider._id,]);
    if (this.routeName === "/program/provider", provider.firstName, provider._id) {
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(["/program/provider", provider.firstName, provider._id]));
    }
  }

  searchActivityByNameDate() {
    this.filterData.categoryId = '';
    this.filterData.lat = '';
    this.filterData.lng = '';
    this.dataservice.setOption(this.filterData);
    this.router.navigate(['/search']);
    if (this.routeName === "/search") {
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(["search"]));
    }
  }

  // Get Current Location Coordinates
  setCurrentLocation() {
    this.mapsAPILoader.load().then(() => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position: any) => {
          this.zoom = 14;
          this.locationData.lat = position.coords.latitude;
          this.locationData.lng = position.coords.longitude;
          this.dataservice.setLocation(this.locationData)
          this.router.navigate(['/search']);
          if (this.routeName === "/search") {
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => this.router.navigate(["search"]));
          }
        });
      } this.geoCoder = new google.maps.Geocoder;
    });
  }
  addKeywordPopularity(id) {
    this.apiservice.addKeywordPopularity(id).subscribe((res: any) => {
    })
  }
  selectSearchedOption(data) {
    if (data.role == 'provider') {
      this.filterData.activityName = "";
      data.name = data.name.toLowerCase();
      data.name = data.name.replace(/ /g, "-");
      data.name = data.name.replace(/\?/g, "-");
      this.router.navigate(["/program/provider", data.name, data._id])
      this.router.navigate(["/program/provider", data.name, data._id,]);
      if (this.routeName === "/program/provider", data.name, data._id) {
        this.router
          .navigateByUrl("/", { skipLocationChange: true })
          .then(() => this.router.navigate(["/program/provider", data.name, data._id]));
      }
    }
    else {
      let filter = ``
      this.addKeywordPopularity(data._id)
      switch (data.keywordType) {
        case 'category':
          filter = `categoryId=${data.keywordValue[0].category.toString()}`
          break;
        case 'subCategory':
          if (data.keywordValue[0].category.length) {
            filter += `tagsIds=${data.keywordValue[0].subcategory.toString()}&categoryId=${data.keywordValue[0].category.toString()}`
          }
          else {
            filter += `tagsIds=${data.keywordValue[0].subcategory.toString()}`
          }
          break;
        case 'age':
          filter = `ageFrom=${data.keywordValue[0].from}&ageTo=${data.keywordValue[0].to}`
          break;
        case 'price':
          filter = `priceFrom=${data.keywordValue[0].from}&priceTo=${data.keywordValue[0].to}`
          break;
        case 'dates':
          filter = `fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}`
          break;
        case 'type':
          filter = `type=${data.keywordValue[0].type.toString()}`
          break;
        case 'time':
          filter = `time=${data.keywordValue[0].time.toString()}`
          break;
        case 'days':
          filter = `day=${data.keywordValue[0].days.toString()}`
          break;
        case 'format':
          filter = `inpersonOrVirtual=${data.keywordValue[0].format.toString()}`
          break;
        case 'topRated':
          filter = `ratingFrom=${data.keywordValue[0].from}&ratingTo=${data.keywordValue[0].to}`
          break;
        case 'isChildDropOff':
          filter += `isChildCare=${data.keywordValue[0].isChildDropOff}`
          break;
        case 'indoorOutdoor':
          filter += `indoorOroutdoor=${data.keywordValue[0].indoorOutdoor}`
          break;

      }
      this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(['/search'], {
          queryParams: {
            filter: filter
          }
        }));
      // this.searchTerm.reset()
    }

  }
  // getAddress() {
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition((position: any) => {
  //       if (position) {
  //         this.lat = position.coords.latitude;
  //         this.lng = position.coords.longitude;
  //       }
  //     },
  //       (error: any) => console.log(error));
  //   } else {
  //     alert("Geolocation is not supported by this browser.");
  //   }
  // }
  getMeters(miles) {
    return miles * 1609.344;
  }
  nearBy(e) {
    let filter = ``;
    if (e.target.checked) {
      switch (+e.target.value) {
        case 10:
          this.distance = 10 * 1609.344;
          filter = `lat=${this.lat}&lng=${this.lng}&nearBy=${this.distance}`
          this.selectedDistance = "Bird’s - eye view";
          break;
        case 5:
          this.distance = 5 * 1609.344;
          filter = `lat=${this.lat}&lng=${this.lng}&nearBy=${this.distance}`
          this.selectedDistance = "Driving (5 mi.)";
          break;
        case 2:
          this.distance = 2 * 1609.344;
          filter = `lat=${this.lat}&lng=${this.lng}&nearBy=${this.distance}`
          this.selectedDistance = "Biking (2 mi.)";
          break;
        case 1:
          this.distance = 1 * 1609.344;
          filter = `lat=${this.lat}&lng=${this.lng}&nearBy=${this.distance}`
          this.selectedDistance = "Walking (1 mi.)";
          break;
        case 500:
          this.distance = 500;
          filter = `lat=${this.lat}&lng=${this.lng}&nearBy=${this.distance}`
          this.selectedDistance = "Within 4 blocks";
          break;
      }
      this.router.navigate(['/search'], {
        queryParams: {
          filter: filter
        }
      })
    }
    else {
      this.router.navigate(['/search'])
    }

    // this.router
    //   .navigateByUrl("/", { skipLocationChange: true })
    //   .then(() => 
    //   ));
  }

  searchKeyword(txt) {
    if (txt) {
      txt += `&parentId=${this.user.id}`
      this.apiservice.searchMultipleKeywords(txt).subscribe((res: any) => {
        const uniqueArry: any = [...new Map(res.data.map((item) => [item["keywordName" && "keywordType"], item])).values()];
        if (uniqueArry) {
          let filter = ``
          for (let data of uniqueArry) {
            switch (data.keywordType) {
              case 'category':
                if (filter) {
                  filter += `&categoryId=${data.keywordValue[0].category.toString()}`
                } else {
                  filter += `categoryId=${data.keywordValue[0].category.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'subCategory':
                if (filter) {
                  if (data.keywordValue[0].category.length) {
                    filter += `&tagsIds=${data.keywordValue[0].subcategory.toString()}&categoryId=${data.keywordValue[0].category.toString()}`
                  }
                  else {
                    filter += `&tagsIds=${data.keywordValue[0].subcategory.toString()}`
                  }
                } else {
                  if (data.keywordValue[0].category.length) {
                    filter += `tagsIds=${data.keywordValue[0].subcategory.toString()}&categoryId=${data.keywordValue[0].category.toString()}`
                  }
                  else {
                    filter += `tagsIds=${data.keywordValue[0].subcategory.toString()}`
                  }

                }
                this.addKeywordPopularity(data._id)
                break;
              case 'age':
                if (filter) {
                  filter += `&ageFrom=${data.keywordValue[0].from}&ageTo=${data.keywordValue[0].to}`
                } else {
                  filter += `ageFrom=${data.keywordValue[0].from}&ageTo=${data.keywordValue[0].to}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'price':
                if (filter) {
                  filter += `&priceFrom=${data.keywordValue[0].from}&priceTo=${data.keywordValue[0].to}`
                } else {
                  filter += `priceFrom=${data.keywordValue[0].from}&priceTo=${data.keywordValue[0].to}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'dates':
                if (filter) {
                  filter += `&fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}`
                } else {
                  filter += `fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'type':
                if (filter) {
                  filter += `&type=${data.keywordValue[0].type.toString()}`
                } else {
                  filter += `type=${data.keywordValue[0].type.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'time':
                if (filter) {
                  filter += `&time=${data.keywordValue[0].time.toString()}`
                } else {
                  filter += `time=${data.keywordValue[0].time.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'days':
                if (filter) {
                  filter += `&day=${data.keywordValue[0].days.toString()}`
                } else {
                  filter += `day=${data.keywordValue[0].days.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'format':
                if (filter) {
                  filter += `&inpersonOrVirtual=${data.keywordValue[0].format.toString()}`
                } else {
                  filter += `inpersonOrVirtual=${data.keywordValue[0].format.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'topRated':
                if (filter) {
                  filter += `&ratingFrom=${data.keywordValue[0].from}&ratingTo=${data.keywordValue[0].to}`
                } else {
                  filter += `ratingFrom=${data.keywordValue[0].from}&ratingTo=${data.keywordValue[0].to}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'isChildDropOff':
                if (filter) {
                  filter += `&isChildCare=${data.keywordValue[0].isChildDropOff}`
                } else {
                  filter += `isChildCare=${data.keywordValue[0].isChildDropOff}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'indoorOutdoor':
                if (filter) {
                  filter += `&indoorOroutdoor=${data.keywordValue[0].indoorOutdoor}`
                } else {
                  filter += `indoorOroutdoor=${data.keywordValue[0].indoorOutdoor}`
                }
                this.addKeywordPopularity(data._id)
                break;

            }
          }
          this.router
            .navigateByUrl("/", { skipLocationChange: true })
            .then(() => this.router.navigate(['/search'], {
              queryParams: {
                filter: filter ? filter : `keyword=${txt}`
              }
            }))
          // this.routeName == '/search' ? this.router
          //   .navigateByUrl("/", { skipLocationChange: true })
          //   .then(() => this.router.navigate(['/search'], {
          //     queryParams: {
          //       filter: filter ? filter : `keyword=${txt}`
          //     }
          //   })) : this.router.navigate(['/search'], {
          //     queryParams: {
          //       filter: filter ? filter : `keyword=${txt}`
          //     }
          //   })


        } else {
          this.router
            .navigateByUrl("/", { skipLocationChange: true })
            .then(() => this.router.navigate(['/search'], {
              queryParams: {
                filter: `keyword=${txt}`
              }
            }))
          // this.routeName == '/search' ? this.router
          //   .navigateByUrl("/", { skipLocationChange: true })
          //   .then(() => this.router.navigate(['/search'], {
          //     queryParams: {
          //       filter: `keyword=${txt}`
          //     }
          //   })) : this.router.navigate(['/search'], {
          //     queryParams: {
          //       filter: `keyword=${txt}`
          //     }
          //   })
        }
      })
      // this.searchTerm.reset()
    }
    else {
      // this.searchTerm.reset()
      this.searchTerm = new FormControl();
      this.routeName == '/search' ? this.router
        .navigateByUrl("/", { skipLocationChange: true })
        .then(() => this.router.navigate(['/search'])) : this.router.navigate(['/search'])
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscribeUser.unsubscribe();
    this.subscribeProgress.unsubscribe();
    if (this.routerEventSub) {
      this.routerEventSub.unsubscribe();
    }
  }
}

import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-footer2",
  template: `
    <footer style="background-color:#fff;">
      <div class="container">
        <div class="footer_inner">
          <div class="row">
            <div class="col-12">
              <!-- <div class="footer_logo">
                <a>
                  <img src="assets/newlogo.png" alt="Logo" />
                </a>
              </div> -->
              <div class="footer_menu">
                <ul>
                  <li class="cursor">
                    <a [routerLink]="['/about-wondrfly']"> About Us</a>
                  </li>
                  <li class="cursor">
                    <a [routerLink]="['/blogs']">Blog</a>
                  </li>
                  <!-- <li class="cursor">
                    <a [routerLink]="['/faq']">FAQ</a>
                  </li> -->
                  <li class="cursor">
                    <a [routerLink]="['/term-condition']">Terms & Conditions</a>
                  </li>
                  <li class="cursor">
                    <a [routerLink]="['/privacy-policy']">Privacy Policy</a>
                  </li>
                  <li class="cursor">
                    <a [routerLink]="['/contactUs']">Contact Us</a>
                  </li>
                </ul>
                <div class="footer-icons cursor">
                <a data-toggle="tooltip" data-placement="bottom" title="Facebook"
                  href="https://www.facebook.com/wondrfly"
                  target="_blank"
                  class="fb"
                >
                  <img src="assets/fb-black.svg" />
                </a>
                <a data-toggle="tooltip" data-placement="bottom" title="Instagram"
                  href="https://www.instagram.com/teamwondrfly"
                  target="_blank"
                  class="insta"
                >
                  <img src="assets/insta-black.svg" />
                </a>
                <a data-toggle="tooltip" data-placement="bottom" title="Twitter"
                  href="https://www.twitter.com/teamwondrfly"
                  target="_blank"
                  class="insta"
                >
                  <img src="assets/twiter-foot-icon.svg" />
                </a>
                <a data-toggle="tooltip" data-placement="bottom" title="Pinterest"
                  href="https://www.pinterest.com/wondrfly/"
                  target="_blank"
                  class="pin"
                >
                  <img src="assets/pin-black.svg" />
                </a>
                <a data-toggle="tooltip" data-placement="bottom" title="Indeed"
                  href="https://www.indeed.com/wondrfly/"
                  target="_blank"
                  class="pin"
                >
                  <img src="assets/indeed-foot-icon.svg" />
                </a>
              </div>
              </div>
              <div class="copyright_text">
                <p>© {{currentYear}} Wondrfly Inc. All Rights Reserved.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  `,
  styleUrls: ["./footer2.component.css"],
})
export class Footer2Component implements OnInit {
  currentYear = new Date().getFullYear()
  constructor() { }

  ngOnInit() { }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientJsonpModule } from '@angular/common/http';
import { MailchimpSubscribeForm } from './mailchimp-subscribe-form.component';

@NgModule({
  declarations: [MailchimpSubscribeForm],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientJsonpModule,
  ],
  exports: [MailchimpSubscribeForm]
})
export class MailchimpSubscribeFormModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { AuthLayoutComponent } from '../layouts/auth-layout/auth-layout.component';
import { UserLayoutComponent } from '../layouts/user-layout/user-layout.component';
import { MarkdownModule } from './markdown/markdown.module';
import { pipesModule } from './pipes/pipes.module';
import { DateFormateModule } from './shared-components/date-format/date-format.module';
import { FooterComponent } from './shared-components/footer/footer.component';
import { Footer2Component } from './shared-components/footer2/footer2.component';
import { HeaderComponent } from './shared-components/header/header.component';
import { Header2Component } from './shared-components/header2/header2.component';
import { RegWallComponent } from './shared-components/reg-wall/reg-wall.component';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';

const components = [
  HeaderComponent,
  FooterComponent,
  Header2Component,
  Footer2Component,
  RegWallComponent,
  UserLayoutComponent,
  AuthLayoutComponent,
]
const thirdPartyModules = [
  MatIconModule,
  MatChipsModule,
  NgxUiLoaderModule,
  DateFormateModule,
  MatAutocompleteModule,
  MatFormFieldModule,
  MatSelectModule,
  MarkdownModule,
  pipesModule,];
@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    ...thirdPartyModules
  ],
  exports: [...thirdPartyModules, ...components]
})
export class SharedModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActiveUser, UserGuard } from './core/guards';
import { AuthLayoutComponent } from './core/layouts/auth-layout/auth-layout.component';
import { UserLayoutComponent } from './core/layouts/user-layout/user-layout.component';
import { PrelodingStrategyService } from './core/services/preloding-strategy.service';

const ROUTES: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  {
    path: '', component: AuthLayoutComponent, canActivate: [ActiveUser],
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/auth/auth-pages.module').then(m => m.AuthPagesModule),
      }
    ]
  },
  {
    path: '',
    component: UserLayoutComponent, canActivate: [UserGuard],
    children: [
      { path: '', loadChildren: () => import('./pages/landing-pages/landing-pages.module').then(m => m.LandingPagesModule), data: { preload: true, loadAfterSeconds: 1 } },
      { path: '', loadChildren: () => import('./pages/search-new/search-new.module').then(m => m.SearchNewModule), data: { preload: true, loadAfterSeconds: 3 } },
      { path: 'parent', loadChildren: () => import('./pages/parent/parent.module').then(m => m.ParentModule), data: { preload: true, loadAfterSeconds: 4 } },
      { path: 'sitemap', loadChildren: () => import('./core/shared/shared-components/sitemap/sitemap.module').then(m => m.SitemapModule) },
      { path: 'program', loadChildren: () => import('./pages/provider/provider.module').then(m => m.ProviderModule), data: { preload: true, loadAfterSeconds: 5 } },
      { path: 'blogs', loadChildren: () => import('./pages/blogs/blogs.module').then(m => m.BlogsModule), },
      { path: '', loadChildren: () => import('./pages/short-urls/short-urls.module').then(m => m.ShortUrlsModule), },
      { path: 'contactUs', loadChildren: () => import('./pages/contact-us/contact-us.module').then(m => m.ContactUsModule) },
    ]
  },
  { path: 'login-parent', loadChildren: () => import('./pages/onboarding/onboarding.module').then(m => m.OnboardingModule), data: { preload: true }, },
  { path: 'faq', loadChildren: () => import('./pages/faq/faq.module').then(m => m.FaqModule) },
  { path: '', loadChildren: () => import('./pages/common-pages/common-pages.module').then(m => m.CommonPagesModule) },
  // { path: '404', component: NotFound404Component },
  // { path: '**', redirectTo: '404' },
]

@NgModule({
  imports: [RouterModule.forRoot(ROUTES, {
    useHash: false, relativeLinkResolution: 'legacy',
    preloadingStrategy: PrelodingStrategyService
  },

  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }

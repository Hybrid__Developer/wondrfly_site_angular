import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { CanonicalService } from './core/shared/canonical.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  envName: string;
  currentOS = null;
  cookiesData: string;
  versionData: string = '1.1.007'
  constructor(
    private canonicalService: CanonicalService,
    private deviceService: DeviceDetectorService,
    private cookies: CookieService) {
    if (environment.name && environment.name !== 'prod') {
      this.envName = environment.name;
    }
    this.cookiesData = this.cookies.get('_ui');
  }
  checkCookieData(data) {
    this.cookiesData = data
  }
  ngOnInit() {
    if (this.versionData !== this.cookies.get('_v')) {
      this.cookies.deleteAll();
      this.cookies.set('_v', this.versionData);
    }
    this.canonicalService.setCanonicalURL();
    this.deviceDetector();
  }
  deviceDetector() {
    this.currentOS = this.deviceService.getDeviceInfo().os;
  }
}

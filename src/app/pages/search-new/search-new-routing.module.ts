import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SavedProgramComponent } from './saved-program/saved-program.component';
import { SearchNewComponent } from './search/search-new.component';

const routes: Routes = [
  { path: 'search', component: SearchNewComponent },
  { path: 'saved', component: SavedProgramComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchNewRoutingModule { }

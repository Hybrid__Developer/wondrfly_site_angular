import { Component, OnInit, ViewChild, ElementRef, OnDestroy, QueryList, ViewChildren, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { LatLngLiteral } from '@agm/core';
import * as moment from 'moment';
import { Category, Child, User } from 'src/app/core/models';
import { DataService } from 'src/app/core/services/dataservice.service ';
import { Meta, Title } from '@angular/platform-browser';
// import { JoyrideService } from 'ngx-joyride';
import { AuthsService } from 'src/app/core/services/auths.service';
import { CookieService } from 'ngx-cookie-service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MapTheme } from 'src/app/core/common/map-theme';
import { createCookies } from 'src/app/core/common/create-cookies';
import { environment } from 'src/environments/environment.prod';
import { FilterClass, FilterIcons, FilterValue } from 'src/app/core/models/filterclass.model';
import { Subscription } from 'rxjs';
import { NavigationService } from 'src/app/core/services/navigation.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'search',
  templateUrl: './search-new.component.html',
  styleUrls: ['./search-new.component.css']
})
export class SearchNewComponent implements OnInit, OnDestroy {
  currentYear = new Date().getFullYear()
  defaultImage = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  public filterClass: FilterClass;
  public filterIcon: FilterIcons;
  public filter: FilterValue;
  errorImage = 'assets/favicon.svg';
  isHideFilter: boolean = false;
  subscribeKeyword: Subscription;
  // isTopFilterCheckBox: boolean = false;
  isMapFilter: boolean = false;
  isSorting: boolean = false;
  isFav: boolean = false;
  isFilter: boolean = false;
  filterObj: any = {}
  activityName: any = ''
  rating: any;
  filterData: any = {};
  locationData: any = {}
  favPrograms: any;
  isMap: boolean = true;
  isLoaded = false
  categories: any[] = [];
  threeCategories: Category[];
  categoriesBySearch: any = new Category;
  isActive: boolean = false
  providersBySearch: any = new User;
  userData: any = {};
  markerUrl = 'assets/location.svg';
  activeMarkerUrl = 'assets/activemarker.svg';
  activeMarkerId = ''
  pageNo: number = 1;
  pageSize: number = 5;
  programs: any = [];
  providerProgram: any = [];
  isInfiniteScrollDisabled: boolean
  isLogin: Boolean = false;
  key: string = '';
  providerRole: boolean = false;
  parentRole: boolean = false;
  favProgramRes: any;
  keyword = 'name';
  searchKey = '';
  isScrol = true;
  fav: any = {
    userId: '',
    programId: '',
  };
  fromDate: any;
  toDate: any;
  fromTime: any;
  toTime: any;
  dateRange: any = {};
  minPrice: any = 50;
  maxPrice: any = 300;
  favourites: any = [];
  totalRating: any = '';
  isRating: boolean
  //  ng5slider start age group
  minAge: number = 0;
  maxAge: number = 17;
  // ng5slider end
  showReset = false;
  deleteProgramRes: any;
  title = 'Search for Online Classes and Programs - Wondrfly';
  // latitude: number = 40.5682945; longitude: number = -74.0409239;
  lat = 40.72652470735903;
  lng = -74.05900394007715;
  zoom = 14;
  address: string;
  private geoCoder;
  isMapMoveChecked: boolean
  coordinates: any = {}
  user = new User
  @ViewChild('search', { static: true })
  public searchElementRef: ElementRef;
  shareUrlSocial = environment.baseUrl;
  baseUrl = environment.baseUrl;
  selectedShareData: any;
  url: string;
  suggested: any = [];
  programOwnerData: any = User
  isOnline: boolean = false;
  isInPerson: boolean = false;
  isOutdoor: boolean = false;
  isIndoor: boolean = false;
  isRating3_5: boolean = false;
  isRating4_5: boolean = false;
  type1: any
  subCats: any = [];
  previous;
  filterName = '';
  selectedCat: any;
  selectedSubCategories: any = [];
  selectedCategories: any = [];
  selectedAgeYears: any = [];
  catData: Category[];
  isBetaPopUp: boolean = false;
  recentFilters: any = []
  searchedSubCategory = ''
  latt: any;
  lngg: any;
  weekDays = [{ name: 'sunday', isSelected: false }, { name: 'monday', isSelected: false }, { name: 'tuesday', isSelected: false }, { name: 'wednesday', isSelected: false }, { name: 'thursday', isSelected: false }, { name: 'friday', isSelected: false }, { name: 'saturday', isSelected: false }]
  durationTimes = [
    { name: '30 min', value: '30', isSelected: false },
    { name: '45 min', value: '45', isSelected: false },
    { name: '60 min', value: '60', isSelected: false },
    { name: '90 min', value: '90', isSelected: false },
    { name: 'Half day', value: 'half', isSelected: false },
    { name: 'Full day', value: 'full', isSelected: false }]
  programTypes = [{ name: 'Private Class', isSelected: false }, { name: 'Classes', isSelected: false }, { name: 'Camps', isSelected: false }, { name: 'Party', isSelected: false }]
  programTimes = [{ name: 'early-morning', isSelected: false, time: '6am - 9am' }, { name: 'morning', isSelected: false, time: '9am - 12pm' }, { name: 'afternoon', isSelected: false, time: '12pm - 3pm' }, { name: 'late-afternoon', isSelected: false, time: '3pm - 6pm' }, { name: 'evening', isSelected: false, time: '6pm - 9pm' }]
  // programTimesShow = ['6am - 9am', '9am - 12pm', '12pm - 3pm', '3pm - 6pm', '6pm - 9pm']
  selectedDays: any = [];
  selectedProgramTypes: any = [];
  selectedDurations: any = [];
  selectedProgramTime: any = [];
  contentLoaded = false;
  fakeLoaderData = [1, 2]
  currentUser: any;
  cookiesData: string;
  activitySearched = 0
  activityClicked = 0
  moment = moment;
  minDate: moment.Moment;
  userId = ''
  upArrow: boolean = false;
  upArrow2: boolean = false;
  providerr = new User;
  activitiesCount = 0
  isChildCareFilter: Boolean;
  topSubCategories: Category[]
  totalItems: any;
  runningFilter: any;
  ageNumbers: any[] = [];
  children: Child[];
  selectedChild: Child = new Child();
  isSeeall: boolean;
  selectedAgeMonths: string;
  blogByCategory: any;
  blogUrl = environment.blogsUrl;
  @HostListener('document:click', ['$event']) clickedOutside($event) {
    // here you can hide your review popup
    this.isSorting = false;
  }
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private apiservice: ApiService,
    private auth: AuthsService,
    private dataservice: DataService,
    private titleService: Title,
    private metaTagService: Meta,
    private cookies: CookieService,
    private navigationService: NavigationService,
    // private joyride: JoyrideService,
    private ngxLoader: NgxUiLoaderService,
    private toast: ToastrService,

    public mapTheme: MapTheme,
    private createCookies: createCookies
  ) {
    this.filterIcon = new FilterIcons();
    this.filterClass = new FilterClass();
    this.filter = new FilterValue();
    this.activitySearched = Number(this.cookies.get('activitySearched'))
    this.countVisit()
    this.minDate = moment();
    this.contentLoaded = false;
    this.userData = auth.userValue;
    this.currentUser = this.userData;
    if (this.userData) {
      this.isLogin = true;
      if (this.userData.role === 'provider') {
        this.providerRole = true;
        this.parentRole = false;
      }
      if (this.userData.role === 'parent') {
        this.userId = this.userData.id
        this.parentRole = true;
        this.providerRole = false;
        this.getChild(this.currentUser.id)
      }
    }
  }

  ageRange() {
    for (let i = 2; i < 18; i++) {
      this.ageNumbers.push(i);
    }
  }

  getChild(id) {
    this.apiservice.getActiveChildByParent(id).subscribe((res: any) => {
      this.children = res.data
    })
  }
  doTogather(data) {
    this.router.navigate(['blogs/category/', "together-activities", "12"])
  }

  searchBySubChild(kid) {
    let filter = ``;
    let id = kid._id ? kid._id : kid.id;
    if (this.filter.activeKid !== id) {
      this.selectedChild = kid;
      let kidTags: any = [];
      this.selectedChild.interestInfo.filter((intrest) => {
        if (intrest.programCount > 0) { kidTags.push(intrest._id) }
      });
      if (kidTags.length > 0) {
        filter = `ageYear=${kid.age}&tagsIds=${kidTags.toString()}`
        this.router.navigate(
          [],
          {
            relativeTo: this.activatedRoute, queryParams: {
              filter: filter,
              kid_id: this.selectedChild.id,
            }
          }
        );
      } else {
        this.toast.error('No intetest added for the Kid!')
      }
    }
    else {
      this.selectedChild.age = '';
      this.selectedAgeYears = [];
      delete this.filterObj['ageYear'];
      delete this.filterObj['tagsIds'];
      this.selectedChild = new Child();
      filter = ``;
      this.runningFilter = null;
      this.router.navigate(
        [],
        { relativeTo: this.activatedRoute, queryParams: {} }
      );
    }
  }

  startTour() {
    // window.scroll(0, 0);
    // if (this.providerProgram.length && Number(this.cookiesData) <= 1) {
    //   this.joyride.startTour({ steps: ['firstStep'] });
    //   this.cookies.set('isTour', '2', 30);
    // }
    // else if (this.cookiesData == '2' && this.providerProgram.length) {
    //   this.joyride.startTour({ steps: ['thirdStep00'] });
    //   this.cookies.set('isTour', '3', 30);
    // }
    // else if (this.cookiesData == '3') {
    //   this.cookies.set('isTour', '4', 30);
    // }
    // this.cookiesData = this.cookies.get('isTour');
  }

  choosedDate(e) {
    if (e.startDate._d != undefined && e.endDate._d != undefined) {
      this.fromDate = e.startDate._d
      this.toDate = e.endDate._d
    }

  }

  centerChange(coords: LatLngLiteral) {
    this.coordinates.lat = coords.lat;
    this.coordinates.lng = coords.lng;
  }
  dragEnd(map) {
    map.setOptions({
      zoomControl: "true",
      zoomControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
      }
    });
    map.addListener("dragend", () => {
      if (this.isMapMoveChecked) {
        this.isMapFilter = true
        this.setFilterQuery('map')
      }
    });
    map.addListener("mousemove", () => {
      if (this.activeMarkerId) {
        this.removeActiveClass(this.activeMarkerId)
      }
    });
  }
  clickedMarker(infowindow) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

  mapClicked(e) {
    this.clickedMarker(e)
  }
  onDayChange(indx: number, day: string, isChecked: boolean) {
    if (isChecked) {
      this.weekDays[indx].isSelected = isChecked
      this.selectedDays.push(day)
      this.setFilterQuery('day')
    } else {
      this.selectedDays.splice(day, -1)
      let el = this.selectedDays.find(itm => itm === day);
      if (el) this.selectedDays.splice(this.selectedDays.indexOf(el), 1);
      this.weekDays[indx].isSelected = false
      this.setFilterQuery('day')
    }
  }
  onDurationChange(indx: number, time: string, isChecked: boolean) {
    if (isChecked) {
      this.weekDays[indx].isSelected = isChecked
      time == "Full day" ? time = "full" : time = time;
      time == "Half day" ? time = "half" : time = time;
      this.selectedDurations.push(time)
      this.setFilterQuery('duration')
    } else {
      this.selectedDurations.splice(time, -1)
      let el = this.selectedDurations.find(itm => itm === time);
      if (el) this.selectedDurations.splice(this.selectedDurations.indexOf(el), 1);
      this.weekDays[indx].isSelected = false
      this.setFilterQuery('duration')
    }
  }
  onProgramTypeChange(indx: number, type: string, isChecked: boolean) {
    if (isChecked) {
      this.programTypes[indx].isSelected = isChecked
      this.selectedProgramTypes.push(type)
      this.setFilterQuery('type')

    } else {
      let pType = type.replace(' ', '+')
      let index = this.selectedProgramTypes.indexOf(type) < 0 ? this.selectedProgramTypes.indexOf(pType) : this.selectedProgramTypes.indexOf(type)
      this.selectedProgramTypes.splice(index, 1);
      this.programTypes[indx].isSelected = false
      this.setFilterQuery('type')

    }
  }
  onProgramFreeChange(isChecked: boolean) {
    if (isChecked) {
      this.filter.priceValue = false;
      this.setFilterQuery('isFree')
    } else {
      this.setFilterQuery('isFree')
    }
  }
  onProgramTimeChange(indx: number, name: any, isChecked: boolean) {
    if (isChecked) {
      this.programTimes[indx].isSelected = isChecked
      this.selectedProgramTime.push(name)
    } else {
      this.selectedProgramTime.splice(name, -1)
      let el = this.selectedProgramTime.find(itm => itm === name);
      if (el) this.selectedProgramTime.splice(this.selectedProgramTime.indexOf(el), 1);
      this.programTimes[indx].isSelected = false
    }
    this.selectedProgramTime = this.selectedProgramTime;
    this.setFilterQuery('time')
  }
  setIndoorOutdoorFilter() {
    this.setFilterQuery('inperson')
  }
  topCategoryCheckActiveFilter(tag) {
    const index = this.selectedSubCategories.indexOf(tag._id);
    if (index >= 0) {
      return true;
    }
    return false
  }
  allSubCategoriesActiveCheck(catIndx) {
    if (this.categories[catIndx].tags.every(r => this.selectedSubCategories.includes(r._id))) {
      return true
    }
    return false
  }
  categoryCheckActiveColappsedFilter(category) {
    category.tags.forEach(tag => {
      const index = this.selectedSubCategories.indexOf(tag._id);
      if (index >= 0) {
        category.collapsed = true
        return true;
      }
      return false
    });

  }
  categoryCheckActiveFilter(id) {
    const index = this.selectedCategories.indexOf(id);
    if (index >= 0) {
      return true;
    }
    return false
    // });

  }
  topCategoryChange(indx) {
    this.searchedSubCategory = this.topSubCategories[indx].name;
    const index = this.selectedSubCategories.indexOf(this.topSubCategories[indx]._id);
    if (index >= 0) {
      this.selectedSubCategories.splice(index, 1);
    }
    else {
      this.selectedSubCategories.push(this.topSubCategories[indx]._id);
    }
    this.setFilterQuery('tagsIds')
  }
  changeAgeYears(indx) {
    this.selectedAgeMonths = '';
    const index = this.selectedAgeYears.indexOf(this.ageNumbers[indx]);
    if (index == -1) {
      this.selectedAgeYears.push(this.ageNumbers[indx]);
      this.setFilterQuery('ageYear')
    }
    else {
      this.selectedAgeYears.splice(index, 1);
      this.setFilterQuery('ageYear')
    }
  }
  changeAgeMonth(months) {
    if (!this.selectedAgeMonths) {
      this.selectedAgeMonths = months;
      this.setFilterQuery('ageMonth');
    } else {
      this.selectedAgeMonths = '';
      this.setFilterQuery('ageMonth');
    }
  }
  activatedActivatedAgeFilter(age) {
    const index = this.selectedAgeYears.indexOf(age);
    if (~index) {
      return true;
    }
    return false
  }
  onProgramsCategoryChange(i) {
    const index = this.selectedCategories.indexOf(this.categories[i].id);
    if (index == -1) {
      this.selectedCategories.push(this.categories[i].id);
      this.setFilterQuery('category')
      for (let tag of this.categories[i].tags) {
        const index = this.selectedSubCategories.indexOf(tag._id);
        if (index == -1) {
          this.selectedSubCategories.push(tag._id);
        }
      }
      this.setFilterQuery('tagsIds')

    }
    else {
      this.selectedCategories.splice(index, 1);
      this.setFilterQuery('category')
    }
  }
  allSubCategoryChange(catIndx, e) {
    if (e.target.checked) {
      for (let tag of this.categories[catIndx].tags) {
        const index = this.selectedSubCategories.indexOf(tag._id);
        if (index == -1) {
          this.selectedSubCategories.push(tag._id);
        }
      }
      this.setFilterQuery('tagsIds')

    } else {
      for (let tag of this.categories[catIndx].tags) {
        const index = this.selectedSubCategories.indexOf(tag._id);
        if (index >= 0) {
          this.selectedSubCategories.splice(index, 1);
        }
      }
      this.setFilterQuery('tagsIds')
    }
  }
  onProgramsSubCategoryChange(i, j, event) {
    // this.categoryId = ''
    // this.categories[i].tags[j].isSelected = event.target.checked;
    if (event.target.checked) {
      this.searchedSubCategory = this.categories[i].tags[j].name;
      const index = this.selectedSubCategories.indexOf(this.categories[i].tags[j]._id);
      if (index == -1) {
        this.selectedSubCategories.push(this.categories[i].tags[j]._id);
      }
      this.setFilterQuery('tagsIds')
    }
    else {
      const index = this.selectedSubCategories.indexOf(this.categories[i].tags[j]._id);
      if (index >= 0) {
        this.selectedSubCategories.splice(index, 1);
      }
      this.setFilterQuery('tagsIds')
    }
  }

  @ViewChildren("types") types: QueryList<ElementRef>;
  clearProgramTypes() {
    this.selectedProgramTypes = []
    this.types.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.setFilterQuery('type')
  }

  @ViewChildren("days") days: QueryList<ElementRef>;
  clearProgramDays() {
    this.selectedDays = []
    this.days.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.setFilterQuery('day')
  }
  @ViewChildren("times") times: QueryList<ElementRef>;
  clearProgramTime() {
    this.selectedProgramTime = []
    this.times.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.setFilterQuery('time')
  }
  mailChimpCompleted() {
    window.scrollTo(0, 0)
    this.cookies.set('exploreModal', '5', 30);
  }
  countVisit() {
    window.scrollTo(0, 0)
    // this.cookiesData = this.cookies.get('isTour');
    let num = Number(this.cookiesData) + 1
    // this.cookies.set('isTour', String(num), 30);
  }
  ngOnInit() {
    // this.getTags()
    this.metaService()
    window.scrollTo(0, 0)
    this.ageRange();
    this.getActiveFilters();
    this.getCategory();
  }

  searchPrice(): void {
    if (this.minPrice < this.maxPrice) {
      this.filter.priceValue = true;
      this.filter.freeValue = false;
      this.setFilterQuery('price')
    }
    else {
      this.toast.info("System should not allow to enter Upper limit less than Lower limit")
    }

  }
  clearPrice() {
    this.minPrice = ''
    this.maxPrice = ''
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
  setFilterQuery(filterType) {
    this.activatedRoute.queryParams
      .subscribe((params: any) => {
        if (params.filter) {
          this.filterObj = JSON.parse('{"' + params.filter.replace(/&/g, '","').replace(/=/g, '":"') + '"}', function (key, value) { return key === "" ? value : decodeURIComponent(value) })
        }
      })
    switch (filterType) {
      case 'category':
        if (this.filterObj.hasOwnProperty('categoryId') && this.selectedCategories.length) {
          this.filterObj.categoryId = this.selectedCategories.toString()
        }
        else if (!this.filterObj.hasOwnProperty('categoryId') && this.selectedCategories.length) {
          Object.assign(this.filterObj, { categoryId: this.selectedCategories.toString() });
        } else {
          delete this.filterObj['categoryId'];
        }
        break;
      case 'tagsIds':
        if (this.filterObj.hasOwnProperty('tagsIds') && this.selectedSubCategories.length) {
          this.filterObj.tagsIds = this.selectedSubCategories.toString();
        }
        else if (!this.filterObj.hasOwnProperty('tagsIds') && this.selectedSubCategories.length) {
          Object.assign(this.filterObj, { tagsIds: this.selectedSubCategories.toString() });
        }
        else {
          delete this.filterObj['tagsIds'];
        }
        break;
      case 'day':
        if (this.filterObj.hasOwnProperty('day') && this.selectedDays.length) {
          this.filterObj.day = this.selectedDays.toString();
        }
        else if (!this.filterObj.hasOwnProperty('day') && this.selectedDays.length) {
          Object.assign(this.filterObj, { day: this.selectedDays.toString() });
        }
        else {
          this.filter.dayValue = false;
          delete this.filterObj['day'];
        }
        break;
      case 'duration':
        if (this.filterObj.hasOwnProperty('duration') && this.selectedDurations.length) {
          this.filterObj.duration = this.selectedDurations.toString();
        }
        else if (!this.filterObj.hasOwnProperty('duration') && this.selectedDurations.length) {
          Object.assign(this.filterObj, { duration: this.selectedDurations.toString() });
        }
        else {
          this.filter.durationValue = false;
          delete this.filterObj['duration'];
        }
        break;

      case 'time':
        if (this.filterObj.hasOwnProperty('time') && this.selectedProgramTime.length) {
          this.filterObj.time = this.selectedProgramTime.toString();
        }
        else if (!this.filterObj.hasOwnProperty('time') && this.selectedProgramTime.length) {
          Object.assign(this.filterObj, { time: this.selectedProgramTime.toString() });
        }
        else {
          delete this.filterObj['time'];
          this.filter.timeValue = false;
        }
        break;

      case 'type':
        let array: any = [];
        array = [...this.selectedProgramTypes]
        // var index = array.indexOf('Drop-ins');
        // if (~index) {
        //   array[index] = 'Drops-in';
        // }

        if (this.filterObj.hasOwnProperty('type') && this.selectedProgramTypes.length) {
          this.filterObj.type = array.toString();
        }
        else if (!this.filterObj.hasOwnProperty('type') && this.selectedProgramTypes.length) {
          Object.assign(this.filterObj, { type: array.toString() });
        }
        else {
          this.filter.typeValue = false;
          delete this.filterObj['type'];
        }
        break;
      case 'rating3_5':
        if (this.filterObj.hasOwnProperty('ratingFrom') && this.filterObj.hasOwnProperty('ratingTo') && this.isRating3_5) {
          this.filterObj.ratingFrom = 3;
          this.filterObj.ratingTo = 5;
        }
        else if (!this.filterObj.hasOwnProperty('ratingFrom') && !this.filterObj.hasOwnProperty('ratingTo') && this.isRating3_5) {
          Object.assign(this.filterObj, { ratingFrom: 3 });
          Object.assign(this.filterObj, { ratingTo: 5 });
        } else {
          delete this.filterObj['ratingFrom']
          delete this.filterObj['ratingTo']
        }
        break;
      case 'rating4_5':
        if (this.filterObj.hasOwnProperty('ratingFrom') && this.filterObj.hasOwnProperty('ratingTo') && this.isRating4_5) {
          this.filterObj.ratingFrom = 4;
          this.filterObj.ratingTo = 5;
        }
        else if (!this.filterObj.hasOwnProperty('ratingFrom') && !this.filterObj.hasOwnProperty('ratingTo') && this.isRating4_5) {
          Object.assign(this.filterObj, { ratingFrom: 4 });
          Object.assign(this.filterObj, { ratingTo: 5 });
        } else {
          delete this.filterObj['ratingFrom']
          delete this.filterObj['ratingTo']
        }
        break;
      case 'highToLow':
        if (this.filterObj.hasOwnProperty('sortBy') && this.filter.highToLowValue) {
          this.filterObj.sortBy = "priceHTL";
        }
        else if (!this.filterObj.hasOwnProperty('sortBy') && this.filter.highToLowValue) {
          Object.assign(this.filterObj, { sortBy: "priceHTL" });
        } else {
          delete this.filterObj['sortBy']
        }
        break;
      case 'lowToHigh':
        if (this.filterObj.hasOwnProperty('sortBy') && this.filter.lowToHighValue) {
          this.filterObj.sortBy = "priceLTH";
        }
        else if (!this.filterObj.hasOwnProperty('sortBy') && this.filter.lowToHighValue) {
          Object.assign(this.filterObj, { sortBy: "priceLTH" });
        } else {
          delete this.filterObj['sortBy']
        }
        break;
      case 'nearestDate':
        if (this.filterObj.hasOwnProperty('sortBy') && this.filter.nearestDateValue) {
          this.filterObj.sortBy = "NearestDate";
        }
        else if (!this.filterObj.hasOwnProperty('sortBy') && this.filter.nearestDateValue) {
          Object.assign(this.filterObj, { sortBy: "NearestDate" });
        } else {
          delete this.filterObj['sortBy']
        }
        break;
      case 'TopRated':
        if (this.filterObj.hasOwnProperty('sortBy') && this.filter.topRated) {
          this.filterObj.sortBy = "TopRated";
        }
        else if (!this.filterObj.hasOwnProperty('sortBy') && this.filter.topRated) {
          Object.assign(this.filterObj, { sortBy: "TopRated" });
        } else {
          delete this.filterObj['sortBy']
        }
        break;
      case 'online':
        delete this.filterObj['indoorOroutdoor']
        if (this.filterObj.hasOwnProperty('inpersonOrVirtual') && this.isOnline) {
          this.filterObj.inpersonOrVirtual = 'online';
        }
        else if (!this.filterObj.hasOwnProperty('inpersonOrVirtual') && this.isOnline) {
          Object.assign(this.filterObj, { inpersonOrVirtual: 'online' });
        } else {
          delete this.filterObj['inpersonOrVirtual']
        }
        break;
      case 'inperson':
        if (this.filterObj.hasOwnProperty('inpersonOrVirtual') && this.isInPerson) {
          this.filterObj.inpersonOrVirtual = 'inperson';
        }
        else if (!this.filterObj.hasOwnProperty('inpersonOrVirtual') && this.isInPerson) {
          Object.assign(this.filterObj, { inpersonOrVirtual: 'inperson' });
        } else {
          delete this.filterObj['indoorOroutdoor']
          delete this.filterObj['inpersonOrVirtual']
        }
        break;
      case 'indoor':
        if (this.filterObj.hasOwnProperty('indoorOroutdoor') && this.isIndoor) {
          this.filterObj.indoorOroutdoor = 'indoor';
        }
        else if (!this.filterObj.hasOwnProperty('inpersonOrViindoorOroutdoor') && this.isIndoor) {
          Object.assign(this.filterObj, { indoorOroutdoor: 'indoor' });
        } else {
          delete this.filterObj['indoorOroutdoor']
        }
        break;
      case 'outdoor':

        if (this.filterObj.hasOwnProperty('indoorOroutdoor') && this.isOutdoor) {
          this.filterObj.indoorOroutdoor = 'outdoor';
        }
        else if (!this.filterObj.hasOwnProperty('indoorOroutdoor') && this.isOutdoor) {
          Object.assign(this.filterObj, { indoorOroutdoor: 'outdoor' });
        } else {
          delete this.filterObj['indoorOroutdoor']
        }
        break;

      case 'date':
        const dateFormat = "YYYY-MM-DD";
        this.fromDate = moment(this.fromDate).format(dateFormat);
        this.toDate = moment(this.toDate).format(dateFormat);
        if (this.fromDate != undefined && this.toDate != undefined) {
          if (this.filterObj.hasOwnProperty('fromDate') && this.filterObj.hasOwnProperty('toDate') && this.toDate.length) {
            this.filterObj.fromDate = this.fromDate;
            this.filterObj.toDate = this.toDate;
          }
          else if (!this.filterObj.hasOwnProperty('fromDate') && !this.filterObj.hasOwnProperty('toDate') && this.toDate.length) {
            Object.assign(this.filterObj, { fromDate: this.fromDate });
            Object.assign(this.filterObj, { toDate: this.toDate });
          }
        }
        else {
          delete this.filterObj['fromDate']
          delete this.filterObj['toDate']
        }
        break;
      case 'ageYear':
        delete this.filterObj['ageMonth']
        if (this.filterObj.hasOwnProperty('ageYear') && this.selectedAgeYears.length) {
          this.filterObj.ageYear = this.selectedAgeYears.toString();
        }
        else if (!this.filterObj.hasOwnProperty('ageYear') && this.selectedAgeYears.length) {
          Object.assign(this.filterObj, { ageYear: this.selectedAgeYears.toString() });
        } else {
          delete this.filterObj['ageYear']

          // delete this.filterObj['ageFrom']
          // delete this.filterObj['ageTo']
        }
        break;
      case 'ageMonth':
        delete this.filterObj['ageYear']
        if (this.filterObj.hasOwnProperty('ageMonth') && this.selectedAgeMonths) {
          this.filterObj.ageMonth = this.selectedAgeMonths.toString();
        }
        else if (!this.filterObj.hasOwnProperty('ageMonth') && this.selectedAgeMonths) {
          Object.assign(this.filterObj, { ageMonth: this.selectedAgeMonths.toString() });
        } else {
          delete this.filterObj['ageMonth']

          // delete this.filterObj['ageFrom']
          // delete this.filterObj['ageTo']
        }
        break;
      case 'childcare':
        if (this.filterObj.hasOwnProperty('isChildCare') && this.isChildCareFilter) {
          Object.assign(this.filterObj, { isChildCare: true });
        }
        else if (!this.filterObj.hasOwnProperty('isChildCare') && this.isChildCareFilter) {
          Object.assign(this.filterObj, { isChildCare: true });

        } else {
          delete this.filterObj['isChildCare']
        }
        break;
      case 'private':
        if (this.filterObj.hasOwnProperty('privateOrGroup') && this.filter.privateGroupValue) {
          Object.assign(this.filterObj, { privateOrGroup: "private" });
        }
        else if (!this.filterObj.hasOwnProperty('privateOrGroup') && this.filter.privateGroupValue) {
          Object.assign(this.filterObj, { privateOrGroup: "private" });

        } else {
          delete this.filterObj['privateOrGroup']
        }
        break;
      case 'price':
        delete this.filterObj['isFree']
        if (this.filterObj.hasOwnProperty('priceFrom') && this.filterObj.hasOwnProperty('priceTo') && this.filter.priceValue) {
          this.filterObj.priceFrom = this.minPrice;
          this.filterObj.priceTo = this.maxPrice;
        }
        else if (!this.filterObj.hasOwnProperty('priceFrom') && !this.filterObj.hasOwnProperty('priceTo') && this.filter.priceValue) {
          Object.assign(this.filterObj, { priceFrom: this.minPrice });
          Object.assign(this.filterObj, { priceTo: this.maxPrice });
        } else {
          delete this.filterObj['priceFrom']
          delete this.filterObj['priceTo']
        }
        break;
      case 'map':
        if (this.filterObj.hasOwnProperty('lat') && this.filterObj.hasOwnProperty('lng') && Object.keys(this.coordinates).length && this.isMapFilter) {
          this.filterObj.lat = this.coordinates.lat;
          this.filterObj.lng = this.coordinates.lng;
        }
        else if (!this.filterObj.hasOwnProperty('lat') && !this.filterObj.hasOwnProperty('lng') && Object.keys(this.coordinates).length && this.isMapFilter) {
          Object.assign(this.filterObj, { lat: this.coordinates.lat });
          Object.assign(this.filterObj, { lng: this.coordinates.lng });
        } else {
          delete this.filterObj['lat']
          delete this.filterObj['lng']
        }
        break;
      case 'isFree':
        delete this.filterObj['priceFrom'];
        delete this.filterObj['priceTo'];
        if (this.filterObj.hasOwnProperty('isFree') && this.filter.freeValue) {
          Object.assign(this.filterObj, { isFree: true });
        }
        else if (!this.filterObj.hasOwnProperty('isFree') && this.filter.freeValue) {
          Object.assign(this.filterObj, { isFree: true });
        } else {
          delete this.filterObj['isFree']
        }
        break;
    }
    const filter = new URLSearchParams(this.filterObj).toString();
    if (this.selectedChild.id) {
      this.router.navigate(
        [],
        {
          relativeTo: this.activatedRoute, queryParams: {
            filter: filter,
            kid_id: this.selectedChild.id,
          }
        }
      );
    }
    else {
      this.router.navigate(
        [],
        {
          relativeTo: this.activatedRoute, queryParams: {
            filter: filter,
          }
        }
      );
    }
  }
  resetFilter() {
    this.dataservice.changeInput('')
    this.selectedChild.id = null
    this.contentLoaded = false
    this.searchedSubCategory = '';
    this.activityName = '';
    this.isInPerson = false;
    this.showReset = false;
    delete this.filterObj['ageYear'];
    delete this.filterObj['tagsIds'];
    this.filter.typeValue = false;
    this.filter.dayValue = false;
    this.filter.timeValue = false;
    this.filter.dateValue = false;
    this.filter.durationValue = false;
    this.filter.priceValue = false;
    this.filter.ageValue = false;
    this.filter.categoryValue = false;
    this.filter.subCategoryValue = false;
    this.filter.ratingValue = false;
    this.selectedCategories = [];
    this.isOnline = false;
    // this.isTopFilterCheckBox = false
    this.isRating3_5 = false;
    this.isRating4_5 = false
    this.isMapFilter = false;
    this.selectedSubCategories = [];
    this.maxAge = 15;
    this.minAge = 0;
    this.pageNo = 1;
    this.pageSize = 5;
    this.selectedProgramTime = []
    this.programs = []
    this.times.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.selectedDays = []
    this.days.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.selectedProgramTypes = []
    this.types.forEach((element) => {
      element.nativeElement.checked = false;
    });
    this.selectedCat = '';
    this.selectedCategories = [];
    this.subCats = [];
    this.selectedSubCategories = [];
    this.categories.map(cat => cat.collapsed = false);
    this.router.navigate(
      [],
      { relativeTo: this.activatedRoute, queryParams: {} }
    );
  }
  clearIntrestFilter() {
    this.selectedChild = new Child();
    this.dataservice.changeInput('')
    this.contentLoaded = false
    this.searchedSubCategory = '';
    this.filter.categoryValue = false;
    this.filter.subCategoryValue = false;
    this.filter.ratingValue = false;
    this.selectedCategories = [];
    this.selectedSubCategories = [];
    this.selectedCat = '';
    this.selectedCategories = [];
    this.subCats = [];
    delete this.filterObj['tagsIds'];
    delete this.filterObj['categoryId'];
    const filter = new URLSearchParams(this.filterObj).toString();
    this.router.navigate(['/search'], {
      queryParams: {
        filter: filter
      }
    });
  }

  goToProgramDetail(data) {
    var programName = data.name;
    programName = programName.toLowerCase();
    programName = programName.replace(/ /g, "-");
    programName = programName.replace(/\?/g, "-");
    programName = programName.replace(/\//g, "-");

    let url = ``
    // if (Object.keys(this.filterObj).length) {
    //   const filter = new URLSearchParams(this.filterObj).toString();
    //   url = `/program/${programName}/${data._id}/${filter}`
    //   return url
    // }
    // else {
    url = `/program/${programName}/${data._id}/filter`
    return url
    // }
  }
  addAction(programId) {
    let regCount = this.activityClicked + 1
    this.cookies.set('activityClicked', String(regCount), 30);
    if (this.parentRole) {
      let body = {
        action: 'click',
        programId: programId
      };
      this.apiservice.addAction(body).subscribe((res: any) => {
      });
    }
  }

  getPublishedProgram(page) {
    // this.contentLoaded = false;
    this.activityName = ''
    this.showReset = false
    this.ngxLoader.start()
    this.suggested = []
    this.apiservice.getPublishedProgramByProvider(page, this.pageSize, 'published').subscribe((res: any) => {
      this.ngxLoader.stop()
      // res.data = res.data.filter(item => item.user.isActivated === true)
      this.programs = res.data;
      if (res.isSuccess) {
        this.totalItems = res.total.totalProviders;
        this.providerProgram = this.programs;
        this.activitiesCount = res.total.publishPrograms;
      }
      this.startTour()
    });
  }

  changePage(page: any) {
    window.scroll(0, 0);
    if (this.showReset) {
      this.programFilter(this.runningFilter, page);
    } else {
      this.getPublishedProgram(page);

    }
  }

  checkCategoryFilter(id, type) {
    if (type === 'category') {
      this.apiservice.getCategory().subscribe((res: any) => {
        let index = res.findIndex(object => {
          return object.id === id;
        });
        if (~index) {
          this.searchedSubCategory = res[index].name
        }
      });
    }
    if (type === 'subcategory') {
      this.apiservice.getTag().subscribe((res: any) => {
        let index = res.data.findIndex(object => {
          return object._id === id;
        });
        if (~index) {
          this.searchedSubCategory = res.data[index].name
        }
      });
    }

  }
  // ---------------------------------------------get categories-------------------------------------
  getCategory() {
    let removedCategory;
    this.apiservice.getCategory().subscribe((res: any) => {
      this.categories = res;
      const idToRemove = '60b47687bb70a952280bfa7b';
      removedCategory = this.categories.filter((item) => item.id === idToRemove);
      this.categories = this.categories.filter((item) => item.id !== idToRemove);
      this.categories.push(removedCategory[0])
      this.categories = this.categories.filter((item) => item.isActivated === true);
      this.catData = this.categories;
      // this.threeCategories = this.categories.slice(0, 3);
    });
  }

  // seeAllCategories() {
  //   this.isSeeall = true;
  //   if (this.threeCategories.length > 3) {
  //     this.threeCategories = this.categories.slice(0, 3);
  //   } else if (this.threeCategories.length == 3) {
  //     this.threeCategories = this.categories;
  //   }
  // }

  // ---------------------------------------------get subCateById-------------------------------------
  getSubCateById(cat, indx) {
    this.categories.map(item => {
      if (this.categories[indx].id !== item.id) {
        return item.collapsed = false
      }
    })
    // this.categoryId = ''
    this.selectedCat = cat.id
    this.selectedSubCategories = []
    this.searchedSubCategory = cat.name
    this.apiservice.getTagByCategoryId(cat.id).subscribe((res: any) => {
      this.subCats = res.data
      this.subCats = this.subCats.filter((item) => item.isActivated === true && item.programCount);
      for (let i in this.subCats) {
        for (let id of this.selectedSubCategories) {
          if (id === this.subCats[i]._id) {
            this.subCats[i].checked = true;
          }
        }
      }
    })
    this.setFilterQuery('category')
  }

  addFavProgram(userId, programId, providerIndx, programIndx) {
    this.providerProgram[providerIndx].programs[programIndx].isFav = true;
    this.fav.userId = userId;
    this.fav.programId = programId;
    this.apiservice.addFavProgram(this.fav).subscribe(res => {
      this.favProgramRes = res;
    });
  }

  deleteFavProgram(favId, providerIndx, programIndx) {
    this.providerProgram[providerIndx].programs[programIndx].isFav = false;
    this.apiservice.deleteFavProgram(favId).subscribe(res => {
      this.deleteProgramRes = res;
    });
  }

  filterByNameDate() {
    this.contentLoaded = false;
    this.filter.typeValue = false;
    this.filter.dayValue = false;
    this.filter.timeValue = false;
    this.filter.dateValue = false;
    this.filter.durationValue = false;
    this.filter.priceValue = false;
    this.filter.ageValue = false;
    this.filter.categoryValue = false;
    this.filter.subCategoryValue = false;
    this.filter.ratingValue = false;
    this.isOnline = false;
    this.isInPerson = false;
    this.apiservice.activityByNameDate(this.activityName).subscribe((res: any) => {
      this.programs = res.data
      this.fakeLoaderData = [1, 2]
      this.contentLoaded = true;
      for (let i in this.programs) {
        let category = this.programs[i].category.filter((v, num, a) => a.findIndex(t => (t.name === v.name)) === num)
        this.programs[i].category = category
      }
      // this.startTour()
      this.showReset = true
      this.searchedSubCategory = this.activityName;
    });
  }
  parentAnalyticAction(key, value) {
    this.apiservice.parentAnalytics(key, this.userId, value).subscribe((res: any) => {
    });
  }
  programFilter(filter, page) {
    // page = 0;
    if ((this.filterObj.hasOwnProperty('keyword'))) {
      this.showReset = true
      this.isLoaded = true
      this.providerProgram = []
      return
    }
    this.ngxLoader.start()
    this.apiservice.programFilter(filter, page, this.pageSize).subscribe((res: any) => {
      this.ngxLoader.stop()
      this.showReset = true
      if (!this.activitySearched && !this.isLogin) {
        this.activitySearched = 1
        this.cookies.set('activitySearched', String(this.activitySearched));
        this.createCookies.createCookie('regWall', 1, 4);
      }
      if (res.isSuccess) {
        this.totalItems = res.total.filterProviders;
        // res.items = res.items.filter(item => item.user.isActivated === true)
        this.programs = res.items;
        if (this.isRating3_5 || this.isRating4_5) {
          this.providerProgram = this.programs.sort((a, b) => b.user?.averageFinalRating - a.user?.averageFinalRating);
        }
        else {
          this.providerProgram = this.programs;
        }
        if (!this.providerProgram.length) {
          this.isLoaded = true
        }
        this.activitiesCount = res.total.filterCount;
        this.startTour()
      }
    });
  }
  searchCategory(key?) {
    this.apiservice.searchTag(key).subscribe((res: any) => {
      this.categoriesBySearch = res;
      this.categoriesBySearch = this.categoriesBySearch.filter((item) => item.isActivated !== false);

    })
  }
  providerSearch(key) {
    this.apiservice.searchUsers(key, 'provider').subscribe((res: any) => {
      this.providersBySearch = res.data;
    })
  }
  goToProviderProfile(provider, scrollToActivities?) {
    if (scrollToActivities === 'activities') {
      this.dataservice.setScrollToActivities(scrollToActivities)
    }
    var providerName = provider.firstName;
    providerName = providerName.toLowerCase();
    providerName = providerName.replace(/ /g, "-");
    providerName = providerName.replace(/\?/g, "-");
    this.router.navigate(['/program/provider', providerName, provider._id]);
  }

  ngOnDestroy() {
    // this.joyride.closeTour()
    // window.document.getElementById("close_modal").click();
    // window.document.getElementById("close_sharemodal").click();
    // this.subscribeKeyword.unsubscribe();
    this.dataservice.changeInput('')
  }

  // ---------------------------------navigate to program detail page -------------------------------------------
  getRating(id, indx) {
    if (this.isRating) {
      this.apiservice.getUserRating(id).subscribe((res: any) => {
        res.finalAverageRating = parseFloat(String(res.finalAverageRating)).toFixed(1)
        this.rating = res
        this.providerProgram[indx].rating = this.rating
      });
    }
    if (this.providerProgram[indx].isCollapsed) {
      this.providerProgram[indx].isCollapsed = false
    }
    else {
      this.providerProgram[indx].isCollapsed = true
    }
  }


  // ---------------------suggested sub categories by sub catids -----------------------
  suggestedSubCategories(id) {
    window.scroll(0, 0)
    this.apiservice.getSuggestedCategory(id).subscribe((res: any) => {
      if (typeof (res) !== 'string') {
        if (!res.error) {
          this.suggested = res
        }
      }
      // this.searchedSubCategory = this.suggested[0].name
      else {
        this.suggested = []
      }
    });
    this.showReset = true;
  }

  removeRecentSearches(type, indx) {
    this.contentLoaded = false;
    switch (type) {
      case 'days':
        this.days.forEach((element) => {
          if (element.nativeElement.defaultValue === this.selectedDays[indx]) {
            return element.nativeElement.checked = false;
          }
        });
        this.selectedDays.splice(indx, 1);
        this.setFilterQuery('day')
        break;

      case 'times':
        this.times.forEach((element) => {
          if (element.nativeElement.value === this.selectedProgramTime[indx]) {
            element.nativeElement.checked = false;
          }
        });
        this.selectedProgramTime.splice(indx, 1);
        this.setFilterQuery('time')
        break;

      case 'types':
        this.types.forEach((element) => {
          if (element.nativeElement.value === this.selectedProgramTypes[indx]) {
            element.nativeElement.checked = false;
          }
        });
        this.selectedProgramTypes.splice(indx, 1);
        this.setFilterQuery('type')
        break;
    }
  }

  scrollLeft(i) {
    document.getElementById('widgetsContent' + i).scrollLeft -= 650;
    // this.checkScroll()
  }

  scrollRight(i) {
    document.getElementById('widgetsContent' + i).scrollLeft += 650;
    // this.checkScroll()
  }


  providerSaveUnsave(providerProgram, providerIndx, isFav) {
    if (isFav) {
      this.removeProvider(providerProgram, providerIndx, isFav);
    } else {
      this.saveProvider(providerProgram, providerIndx, isFav);
    }
  }

  //  save provider
  saveProvider(providerProgram, providerIndx, isFav) {
    let model = {
      parent: this.userId,
      provider: providerProgram[providerIndx].user._id
    }
    this.apiservice.saveProvider(model).subscribe((res: any) => {
      if (res.isSuccess) {
        this.providerProgram[providerIndx].user.isFav = true;
        this.providerProgram[providerIndx].programs.map(p => p.isFav = true);
      }
    });
  }

  removeProvider(providerProgram, providerIndx, isFav) {
    this.apiservice.unsaveProviders(providerProgram[providerIndx].user._id).subscribe((res: any) => {
      if (res.isSuccess) {
        this.providerProgram[providerIndx].user.isFav = false;
        this.providerProgram[providerIndx].programs.map(p => p.isFav = false);
      }
    });
  }

  metaService() {
    this.apiservice.getMetaServiceByPageName('search').subscribe(res => {
      if (res.isSuccess) {
        if (res.data !== null) {
          this.titleService.setTitle(res.data.title);
          this.metaTagService.updateTag(
            { name: 'description', content: res.data.description }
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: res.data.keywords }
          );
        }
        else {
          this.titleService.setTitle(this.title);
          this.metaTagService.updateTag(
            { name: 'description', content: `Looking for some easy and fun summer activities for your kids? By visiting Wondrfly's search page you can find best programs or classes. ` }
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: 'kid friendly search,kids activities search, kids programs search' }
          );
        }
      }
      else {
        this.titleService.setTitle(this.title);
        this.metaTagService.updateTag(
          { name: 'description', content: `Looking for some easy and fun summer activities for your kids? By visiting Wondrfly's search page you can find best programs or classes. ` }
        );
        this.metaTagService.addTag(
          { name: 'keywords', content: 'kid friendly search,kids activities search, kids programs search' }
        );
      }
    })

  }
  getTags() {
    this.apiservice.getTag().subscribe((res: any) => {
      this.topSubCategories = res.data.filter(res => res.isActivated && res.programCount)
      this.topSubCategories.sort(function (a: any, b: any) { return b.programCount - a.programCount });
    });
  }
  removeActiveClass(id) {
    document.getElementById(id).className = "search-results-section"
    document.getElementById(id).classList.remove("active-scroll-marker")
  }
  onMarkerClick(id) {
    this.activeMarkerId = id
    document.getElementById(id).className = "active-scroll-marker"
    document.getElementById(id).scrollIntoView({ behavior: 'smooth', block: 'start' });
  }
  checkProgramDays(days) {
    for (let i in days)
      if (days[i]) return true
    return false
  }
  detectDots(ele) {
    return ele.offsetHeight < ele.scrollHeight ||
      ele.offsetWidth < ele.scrollWidth;
  }

  sorting(sort: string) {
    this.isSorting = false;
    switch (sort) {
      case 'TopRated':
        // if (!this.isRating4_5) {
        //   this.isRating4_5 = true;
        //   this.filterIcon.ratingIcon = true;
        //   this.setFilterQuery('rating4_5')
        // } else {
        //   this.isRating4_5 = false;
        //   this.setFilterQuery('rating4_5')
        // }
        // if (!this.filter.highToLowValue) {
        this.filter.topRated = true;
        this.filter.highToLowValue = false;
        this.filter.lowToHighValue = false;
        this.setFilterQuery('TopRated')
        // }
        //  else {
        //   this.filter.topRated = false;
        //   this.setFilterQuery('TopRated')
        // }
        break;
      case 'highToLow':
        if (!this.filter.highToLowValue) {
          this.filter.topRated = false;
          this.filter.lowToHighValue = false;
          this.filter.highToLowValue = true;
          this.setFilterQuery('highToLow')
        } else {
          this.filter.highToLowValue = false;
          this.setFilterQuery('highToLow')
        }
        break;
      case 'lowToHigh':
        if (!this.filter.lowToHighValue) {
          this.filter.topRated = false;
          this.filter.lowToHighValue = true;
          this.filter.highToLowValue = false;
          this.setFilterQuery('lowToHigh')
        } else {
          this.filter.lowToHighValue = false;
          this.setFilterQuery('lowToHigh')
        }
        break;
      case 'nearestDate':
        if (!this.filter.nearestDateValue) {
          this.filter.nearestDateValue = true;
          this.setFilterQuery('nearestDate')
        } else {
          this.filter.nearestDateValue = false;
          this.setFilterQuery('nearestDate')
        }
        break;
    }
  }

  getActiveFilters() {
    this.activatedRoute.queryParams
      .subscribe((params: any) => {
        if (params.kid_id) {
          this.filterClass.kidActiveClass = true;
          this.filter.activeKid = params.kid_id;
        } else {
          this.filterClass.kidActiveClass = false;
          this.filter.activeKid = "";
        }
        if (params.filter) {
          this.filterObj = JSON.parse('{"' + params.filter.replace(/&/g, '","').replace(/=/g, '":"') + '"}', function (key, value) { return key === "" ? value : decodeURIComponent(value) })
          if (this.filterObj.hasOwnProperty('categoryId')) {
            this.filter.categoryValue = true;
            this.filterIcon.childCareIcon = true;
            this.filterClass.categoryClass = true;
            this.selectedCategories = this.filterObj.categoryId.split(',');
            this.checkCategoryFilter(this.selectedCategories[0], 'category')
            // this.selectedCat = this.categoryId
            for (let id of this.selectedCategories) {
              this.parentAnalyticAction('subCategory', id)
            }
          }
          // else {
          //   this.isCategoryFilter = false;
          // }
          if (this.filterObj.hasOwnProperty('tagsIds')) {
            this.filter.categoryValue = true;
            this.filterIcon.categoryIcon = true;
            this.filterClass.categoryClass = true;
            let ids = this.filterObj.tagsIds.split(',');
            this.selectedSubCategories = ids;
            this.checkCategoryFilter(this.selectedSubCategories[0], 'subcategory')
            for (let id of ids) {
              this.parentAnalyticAction('subCategory', id)
            }
          }
          if (this.filterObj.hasOwnProperty('day')) {
            this.filterIcon.dateIcon = true;
            this.filterClass.dateClass = true;
            let days = this.filterObj.day.split(',');
            this.selectedDays = days;
            this.weekDays.map(element => {
              let name = element.name
              if (this.selectedDays.includes(name)) {
                element.isSelected = true
              }
            })
          }
          // else {
          //   // this.isDatedClass = false;
          //   this.isDaysFilter = false
          // }
          if (this.filterObj.hasOwnProperty('duration')) {
            this.filter.durationValue = true;
            this.filterIcon.durationIcon = true;
            this.filterClass.durationClass = true;
            let durations = this.filterObj.duration.split(',');
            this.selectedDurations = durations;
            this.durationTimes.map(element => {
              let value = element.value
              if (this.selectedDurations.includes(value)) {
                element.isSelected = true
              }
            })
          }
          // else {
          //   // this.isDatedClass = false;
          //   this.isDurationsFilter = false
          // }
          if (this.filterObj.hasOwnProperty('time')) {
            this.filter.timeValue = true;
            this.filterIcon.timeIcon = true;
            this.filterClass.timeClass = true;
            let time = this.filterObj.time.split(',');
            this.selectedProgramTime = time
            this.programTimes.map(element => {
              let name = element.name
              // let value = element.value
              if (this.selectedProgramTime.includes(name)) {
                element.isSelected = true
              }
            });
          }
          // else {
          //   this.isTimeClass = false
          //   this.isTimeFilter = false
          // }
          if (this.filterObj.hasOwnProperty('type')) {
            this.filter.typeValue = true;
            let type = this.filterObj.type.split(',');
            // var index = type.indexOf('Drops-in');
            // if (~index) {
            //   type[index] = 'Drop-ins';
            // }
            for (let item of type) {
              item = item.replace('+', ' ')
              this.filterClass.typeClass = true;
              let index = this.programTypes.findIndex((t) => t.name == item)
              this.programTypes[index].isSelected = true
            }

            this.selectedProgramTypes = type
            this.programTypes.map(element => {
              let name = element.name
              if (this.selectedProgramTypes.includes(name)) {
                element.isSelected = true
                this.filterClass.typeClass = true;
                this.filterIcon.typeIcon = true;
              }
            })
          }
          else {
            this.filterClass.typeClass = false;
            this.filter.typeValue = false
          }
          if (this.filterObj.hasOwnProperty('ratingFrom') && this.filterObj.hasOwnProperty('ratingTo')) {
            this.filterClass.ratingClass = true;
            this.filterIcon.ratingIcon = true;
            if (+this.filterObj.ratingFrom >= 4) {
              this.isRating4_5 = true
            } else if (+this.filterObj.ratingFrom > 0) {
              this.isRating3_5 = true
            }

          }
          else {
            this.filterClass.ratingClass = false;
            this.isRating3_5 = false;
            this.isRating4_5 = false
          }
          if (this.filterObj.hasOwnProperty('privateOrGroup')) {
            this.filterClass.privateGroupClass = true;
            this.filterIcon.privateGroupIcon = true;
            if (this.filterObj.privateOrGroup == 'private') {
              this.filter.privateGroupValue = true;
            }
          }
          else {
            this.filterClass.privateGroupClass = false;
            this.filter.privateGroupValue = false;
          }
          if (this.filterObj.hasOwnProperty('inpersonOrVirtual')) {
            this.filterIcon.formatIcon = true;
            this.filterClass.formatClass = true;
            if (this.filterObj.inpersonOrVirtual == 'online') {
              this.isOnline = true;
              this.filter.formatValue = true;
            }
            else if (this.filterObj.inpersonOrVirtual == 'inperson') {
              this.isInPerson = true
              this.filter.formatValue = true;
            }
          }
          else {
            this.isOnline = false;
            this.isInPerson = false;
            this.filter.formatValue = false;
          }
          if (this.filterObj.hasOwnProperty('indoorOroutdoor')) {
            Object.assign(this.filterObj, { inpersonOrVirtual: 'inperson' });
            this.isInPerson = true
            this.isOnline = false
            if (this.filterObj.indoorOroutdoor == 'indoor') {
              this.isIndoor = true;
            }
            else if (this.filterObj.indoorOroutdoor == 'outdoor') {
              this.isOutdoor = true;
            }
          }
          else {
            this.isIndoor = false;
            this.isOutdoor = false;
          }
          if (this.filterObj.hasOwnProperty('fromDate') && this.filterObj.hasOwnProperty('toDate')) {
            this.filter.dateValue = true;
            this.filterIcon.dateIcon = true;
            this.filterClass.dateClass = true;
            this.fromDate = this.filterObj.fromDate
            this.toDate = this.filterObj.toDate
          }
          // else {
          //   // this.isDatedClass = false;
          //   this.isDateFilter = false;
          // }
          if (this.filterObj.hasOwnProperty('ageYear')) {
            this.filter.ageValue = true;
            this.filterIcon.ageIcon = true;
            this.filterClass.ageClass = true;
            this.selectedAgeYears = this.filterObj.ageYear.split(',');
            this.selectedAgeYears = this.selectedAgeYears.map(Number)
            // this.maxAge = +this.filterObj.ageTo
          }
          // else {
          //   this.isAgeClass = false;
          //   this.isAgeFilter = false;
          // }
          if (this.filterObj.hasOwnProperty('ageMonth')) {
            this.filter.ageValue = true;
            this.filterIcon.ageIcon = true;
            this.filterClass.ageClass = true;
            this.selectedAgeMonths = this.filterObj.ageMonth;
            // this.maxAge = +this.filterObj.ageTo
          }
          // else {
          //   this.isAgeClass = false;
          //   this.isAgeFilter = false;
          // }
          if (this.filterObj.hasOwnProperty('priceFrom') && this.filterObj.hasOwnProperty('priceTo')) {
            this.minPrice = this.filterObj.priceFrom
            this.maxPrice = this.filterObj.priceTo
            this.filter.priceValue = true;
            this.filterIcon.priceIcon = true;
            this.filterClass.priceClass = true;
          }
          // else {
          //   this.isPriceFilter = false;
          //   this.isPriceClass = false;
          // }
          if (this.filterObj.hasOwnProperty('lat') && this.filterObj.hasOwnProperty('lng')) {
            this.isMapFilter = true
            this.coordinates.lat = +this.filterObj.lat;
            this.coordinates.lng = +this.filterObj.lng;
          }
          else {
            this.isMapFilter = false;
          }
          if (this.filterObj.hasOwnProperty('isChildCare')) {
            this.filter.childCareValue = true;
            this.filterIcon.childCareIcon = true;
            this.filterClass.childCareClass = true;
          }
          // else {
          //   this.isChildCareFilter = false
          // }
          if (this.filterObj.hasOwnProperty('isFree')) {
            this.filter.freeValue = true;
            this.filterIcon.priceIcon = true;
            this.filterClass.priceClass = true;
          }
          // else {
          //   this.isFreeFilter = false
          // }
          this.runningFilter = params.filter
          this.programFilter(params.filter, this.pageNo)
        }
        else {
          this.router.navigate(
            [],
            { relativeTo: this.activatedRoute, queryParams: {} }
          );
          this.getPublishedProgram(this.pageNo);
          this.selectedCategories = [];
          this.subCats = [];
          this.selectedSubCategories = [];
          this.selectedAgeYears = [];
          this.selectedAgeMonths = '';
          this.filter.ageValue = false;
          this.filterIcon.ageIcon = false;
          this.selectedChild = new Child();
          this.filterClass.ageClass = false;
          this.runningFilter = null;
          this.categories.map(cat => cat.collapsed = false)
        }
      })
  }

}

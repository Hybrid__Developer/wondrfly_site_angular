import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchNewRoutingModule } from './search-new-routing.module';
import { SearchNewComponent } from './search/search-new.component';
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule } from '@angular/forms';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { AgmCoreModule } from '@agm/core';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { RatingModule } from 'src/app/core/shared/shared-components/rating/rating.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { SavedProgramComponent } from './saved-program/saved-program.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import { CustomDirectivesModule } from 'src/app/core/shared/custom-directives/custom-directives.module';

@NgModule({
  declarations: [SearchNewComponent, SavedProgramComponent],
  imports: [
    CommonModule,
    SearchNewRoutingModule,
    CoreModule,
    RatingModule,
    FormsModule,
    NgxDaterangepickerMd.forRoot(),
    AgmCoreModule,
    NgxSliderModule,
    // JoyrideModule,
    DragScrollModule,
    CustomDirectivesModule,
    NgxPaginationModule,
  ]
})
export class SearchNewModule { }

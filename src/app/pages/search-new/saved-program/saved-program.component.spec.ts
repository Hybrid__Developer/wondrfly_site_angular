import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedProgramComponent } from './saved-program.component';

describe('SavedProgramComponent', () => {
  let component: SavedProgramComponent;
  let fixture: ComponentFixture<SavedProgramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavedProgramComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

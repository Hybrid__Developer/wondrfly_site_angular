import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Subscription } from 'rxjs';
import { MapTheme } from 'src/app/core/common/map-theme';
import { User } from 'src/app/core/models';
import { ApiService } from 'src/app/core/services/api.service.service';
import { AuthsService } from 'src/app/core/services/auths.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-saved-program',
  templateUrl: './saved-program.component.html',
  styleUrls: ['./saved-program.component.css']
})
export class SavedProgramComponent implements OnInit {
  defaultImage = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  baseUrl = environment.baseUrl;
  isLoaded = false;
  savedProvider: any = [];
  rating: any;
  currentUser: User;
  subscribeUser: Subscription;
  selectedShareData: any;
  parentRole: boolean = false;
  isMap: boolean = false;
  lat = 40.72652470735903;
  lng = -74.05900394007715;
  zoom = 14;
  previous;
  markerUrl = 'assets/location.svg';
  activeMarkerUrl = 'assets/activemarker.svg';

  constructor(private authService: AuthsService,
    private apiservice: ApiService,
    private ngxLoader: NgxUiLoaderService,
    private router: Router,
    public mapTheme: MapTheme,) {
    this.currentUser = authService.userValue;
    this.subscribeUser = this.authService.userData.subscribe(res => {
      this.currentUser = res;
    })
  }
  ngOnInit(): void {
    this.savedProviders()
  }

  getRating(id) {
    this.apiservice.getUserRating(id).subscribe((res: any) => {
      this.rating = res
      this.rating.finalAverageRating = parseFloat(String(this.rating.finalAverageRating)).toFixed(1)
    });
  }

  // saved providers list 
  savedProviders() {
    this.ngxLoader.start();
    this.isLoaded = false
    this.apiservice.getSavedProvidersByParentId(this.currentUser.id).subscribe((res: any) => {
      this.ngxLoader.stop();
      this.savedProvider = res.data;
      this.isLoaded = true
    });
    this.ngxLoader.stop();
  }

  removeFav(programId, providerIndex, programIndex) {
    this.apiservice.deleteFavProgram(programId).subscribe((res: any) => {
      if (res.isSuccess) {
        if (res.isSuccess) {
          if (programIndex > -1) { // only splice array when item is found
            this.savedProvider[providerIndex].favourites.splice(programIndex, 1); // 2nd parameter means remove one item only
          }
        }
      }
    });
  }

  removeFavProvider(providerId, providerIndx) {
    this.apiservice.unsaveProviders(providerId).subscribe((res: any) => {
      if (res.isSuccess) {
        if (providerIndx > -1) { // only splice array when item is found
          this.savedProvider.splice(providerIndx, 1); // 2nd parameter means remove one item only
        }
      }
    });
  }
  scrollLeft(i) {
    document.getElementById('widgetsContent' + i).scrollLeft -= 650;
    // this.checkScroll()
  }

  scrollRight(i) {
    document.getElementById('widgetsContent' + i).scrollLeft += 650;
    // this.checkScroll()
  }

  // ----------------------------------------add action-------------------------------------------
  addAction(programId) {
    let body = {
      action: 'click',
      programId: programId
    };
    this.apiservice.addAction(body).subscribe((res: any) => {
    });
  }

  // ---------------------------------navigate to program detail page -------------------------------------------
  goToProgramDetail(data) {
    if (this.parentRole) {
      this.addAction(data._id);
    }
    data.name = data.name.replace(/ /g, "-");
    data.name = data.name.replace(/\?/g, "-");
    this.router.navigate(['program', data.name, data._id, 'filter']);
  }

  goToProviderProfile(provider) {
    var providerName = provider.firstName;
    providerName = providerName.toLowerCase();
    providerName = providerName.replace(/ /g, "-");
    providerName = providerName.replace(/\?/g, "-");
    this.router.navigate(['/program/provider', providerName, provider._id]);

  }

  // dragEnd(map) {
  //   map.addListener("dragend", () => {
  //     if (this.isMapMoveChecked) {
  //       // this.isMap = true
  //       // this.setFilterQuery('map')
  //     }
  //   });
  // }
  clickedMarker(infowindow) {
    if (this.previous) {
      this.previous.close();
    }
    this.previous = infowindow;
  }

  mapClicked(e) {
    this.clickedMarker(e)
  }

  checkProgramDays(days) {
    for (let i in days)
      if (days[i]) return true
    return false
  }
  detectDots(ele) {
    return ele.offsetHeight < ele.scrollHeight ||
      ele.offsetWidth < ele.scrollWidth;
  }
}

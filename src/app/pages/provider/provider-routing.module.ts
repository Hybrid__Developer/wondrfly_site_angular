import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProgramProviderComponent } from './program-provider/program-provider.component';
import { NewDetailComponent } from './program/new-detail/new-detail.component';
import { ProviderComponent } from './provider.component';
const routes: Routes = [
  {
    path: '', component: ProviderComponent, children: [
      {
        path: 'provider/:name/:id', component: ProgramProviderComponent
      },
      {
        path: ':title/:id/:filter',
        component: NewDetailComponent,
      },
    ]
  }

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderRoutingModule { }

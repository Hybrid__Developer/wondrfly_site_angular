import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProviderRoutingModule } from './provider-routing.module';
import { CoreModule } from 'src/app/core/core.module';
import { ProgramProviderComponent } from './program-provider/program-provider.component';
import { ProviderComponent } from './provider.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule } from '@angular/forms';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { RatingModule } from 'src/app/core/shared/shared-components/rating/rating.module';
import { NewDetailComponent } from './program/new-detail/new-detail.component';
import { sharePopupModule } from 'src/app/core/shared/shared-components/share-popup/share-popup.module';
import { CustomDirectivesModule } from 'src/app/core/shared/custom-directives/custom-directives.module';
@NgModule({
  declarations: [ProviderComponent, ProgramProviderComponent, NewDetailComponent],
  imports: [
    CommonModule,
    ProviderRoutingModule,
    CoreModule,
    RatingModule,
    AgmCoreModule,
    sharePopupModule,
    FormsModule,
    CustomDirectivesModule,
    NgxDaterangepickerMd.forRoot(),
    NgxSliderModule]
})
export class ProviderModule { }

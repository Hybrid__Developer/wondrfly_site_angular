import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { ApiService } from "src/app/core/services/api.service.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { LocalStorageService } from "src/app/core/services";
import { User } from "src/app/core/models";
import { environment } from "src/environments/environment";
import { Meta, Title } from "@angular/platform-browser";
import { ToastrService } from "ngx-toastr";
import { AuthsService } from "src/app/core/services/auths.service";
import { DeviceDetectorService } from 'ngx-device-detector';
import { BlogApiService } from "src/app/core/services/blog-api.service";
// import { User } from '../../core/models/index'
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  currentYear = new Date().getFullYear()
  defaultImage = "https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif";
  errorImage = "assets/about-beta1.png";
  blogUrl = environment.blogsUrl;
  signinForm: FormGroup;
  credentials = {
    email: "",
    password: "",
    browserName: "",
    ipAddress: "",
    osName: ""
  };
  isLoading = false;
  hide: boolean = true;
  response: any;
  message: string = "Logged In Successfully!";
  action: boolean = true;
  activeDeactiveResponse: any;
  isModal = false;
  signInImage = "";
  signInImages = [
    "assets/preOnboarding/1.jpg",
    "assets/preOnboarding/2.jpg",
    "assets/preOnboarding/3.jpg",
    "assets/preOnboarding/4.jpg",
    "assets/preOnboarding/5.jpg",
    "assets/preOnboarding/6.jpg",
    "assets/preOnboarding/7.jpg",
    "assets/preOnboarding/8.jpg",
    "assets/preOnboarding/9.jpg",
    "assets/preOnboarding/10.jpg",
    "assets/preOnboarding/11.jpg",
  ];
  user: any = new User();
  title = "Parent Log in | Find Online Kids Classes - Wondrfly";
  constructor(
    private router: Router,
    private auth: AuthsService,
    private apiservice: ApiService,
    private titleService: Title,
    private metaTagService: Meta,
    private ngxLoader: NgxUiLoaderService,
    private toastr: ToastrService,
    public store: LocalStorageService,
    private blogservice: BlogApiService,
    private deviceService: DeviceDetectorService
  ) { }
  onPassword() {
    this.hide = !this.hide;
  }
  // cancel(){
  //   this.router.navigate(["/"]);
  // }

  randomImage() {
    const num = Math.floor(Math.random() * this.signInImages.length);
    this.signInImage = this.signInImages[num];
  }

  signin() {
  //  this.credentials.systemDetail = this.systemDetail
    localStorage.removeItem("userId");
    this.ngxLoader.start();
    this.credentials.email = this.credentials.email.toLocaleLowerCase()
    this.auth.login(this.credentials).subscribe((res: any) => {
      this.ngxLoader.stop();
      this.user = res.data;
      if (res.isSuccess) {
        switch (true) {
                    case this.user.role==='parent' && this.user.isOnBoardingDone:
                      this.auth.setUser(this.user);
                      this.router.navigate(["my-wondrfly"]);
                      setTimeout(() => {
                        this.blogservice.strapiLogin();
                      }, 2000);
                      this.blogservice.strapiLogin();
                      break;
                      case this.user.role==='parent' && !this.user.isOnBoardingDone:
                        this.auth.setUser(this.user);
                      this.router.navigate(["login-parent"]);
                      setTimeout(() => {
                        this.blogservice.strapiLogin();
                      }, 2000);                      break;
                    case this.user.role==='provider':
                      this.auth.setUser(this.user);
                      this.router.navigate(["profile", this.user.id]);
                      setTimeout(() => {
                        this.blogservice.strapiLogin();
                      }, 2000);                      break;
                    case this.user.role==='superAdmin':
                      this.toastr.error("Please Login As Provider Or Parent Only!");
                      break;
                    default:
                      break;
                  }
      } else {
        this.toastr.error(res.error);
      }
      this.ngxLoader.stop();
    });
  }

  activeDeactiveUser() {
    var booleanValue = true;
    this.ngxLoader.start();
    this.apiservice
      .activeDeactiveUser(this.response.data.id, booleanValue)
      .subscribe((activeDeactiveResponse: any) => {
        this.ngxLoader.stop();
        if (activeDeactiveResponse) {
          return this.signin();
        } else {
          this.toastr.error(activeDeactiveResponse.error);
        }
      });
    this.ngxLoader.stop();
  }
  onForgotPassword() {
    this.router.navigate(["/forgot-password"]);
  }
  ngOnInit() {
    this.detectDeviceDetail();
    this.getIPAddress();
    this.titleService.setTitle(this.title);
    this.metaTagService.updateTag({
      name: "description",
      content:
        "Log in now to your Wondrfly parent account. Don't have an account? Log In With Facebook.Or create an account.",
    });
    this.metaTagService.addTag({
      name: "keywords",
      content: "New Member Login",
    });
    window.scroll(0, 0);
    this.randomImage();
    this.signinForm = new FormGroup({
      username: new FormControl("", [Validators.required]),
      password: new FormControl("", [Validators.required, Validators.min(1)]),
      // rememberMe: new FormControl(false)
    });
  }
  // ---------getIPAddress-------

  getIPAddress() {
    this.apiservice.getIPAddress().subscribe((res: any) => {
      this.credentials.ipAddress = res.ip;
    });
  }
  // ---------detectBrowserName-------
  detectDeviceDetail() {
    let deviceInfo = this.deviceService.getDeviceInfo();
    this.credentials.browserName = deviceInfo.browser;
    this.credentials.osName = deviceInfo.os;
    // const isMobile = this.deviceService.isMobile();
    //       const isTablet = this.deviceService.isTablet();
    //       const isDesktopDevice = this.deviceService.isDesktop();
  }
}

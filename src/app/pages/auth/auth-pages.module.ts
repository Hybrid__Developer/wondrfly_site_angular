import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthPagesRoutingModule } from './auth-pages-routing.module';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AskToJoinComponent } from './ask-to-join/ask-to-join.component';
import { SignUpGuardianComponent } from './sign-up-guardian/sign-up-guardian.component';
import { CoreModule } from 'src/app/core/core.module';

@NgModule({
  declarations: [
    LoginComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    AskToJoinComponent,
    SignUpGuardianComponent],
  imports: [
    CommonModule,
    AuthPagesRoutingModule,
    ReactiveFormsModule,
    CoreModule
  ]
})
export class AuthPagesModule { };

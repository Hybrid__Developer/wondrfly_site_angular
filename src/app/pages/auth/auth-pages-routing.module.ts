import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingNewComponent } from '../landing-pages/landing-new/landing-new.component';
import { AskToJoinComponent } from './ask-to-join/ask-to-join.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'join-the-beta', component: AskToJoinComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'landing', component: LandingNewComponent }

  // {path:'login',component:LoginComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthPagesRoutingModule { }

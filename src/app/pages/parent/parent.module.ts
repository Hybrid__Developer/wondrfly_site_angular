import { NgModule } from '@angular/core';
import { ParentRoutingModule } from './parent.routing';
import { CoreModule } from '../../core/core.module';
import { CommonModule } from '@angular/common';
import { CustomFormsModule } from 'ng2-validation';
import { ParentProfileComponent } from './parent-profile/parent-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { NgxMaskModule } from 'ngx-mask';
import { ParentComponent } from './parent.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { RatingModule } from 'src/app/core/shared/shared-components/rating/rating.module';
import { sharePopupModule } from 'src/app/core/shared/shared-components/share-popup/share-popup.module';
import { DragScrollModule } from 'ngx-drag-scroll';


@NgModule({
    entryComponents: [],
    declarations: [
        ParentComponent,
        ParentProfileComponent,
        // SuggestionComponent
    ],
    imports: [
        CommonModule,
        ParentRoutingModule,
        FormsModule,
        CustomFormsModule,
        ReactiveFormsModule,
        NgxMaskModule.forRoot(),
        CoreModule,
        AgmCoreModule,
        sharePopupModule,
        RatingModule,
        DragScrollModule,
        NgCircleProgressModule.forRoot({
            // set defaults here
            radius: 100,
            outerStrokeWidth: 16,
            innerStrokeWidth: 8,
            outerStrokeColor: "#78C000",
            innerStrokeColor: "#C7E596",
            animationDuration: 300,
        })
    ],
})
export class ParentModule { }

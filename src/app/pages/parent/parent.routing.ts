import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParentProfileComponent } from './parent-profile/parent-profile.component';
import { Role } from 'src/app/core/models/role.model';
import { ParentComponent } from './parent.component';
export const routes: Routes = [
  {
    path: '', component: ParentComponent,
    children: [
      {
        path: 'profile/:id', component: ParentProfileComponent,
        data: {
          roles: [
            Role.Parent,
          ]
        }
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParentRoutingModule { }




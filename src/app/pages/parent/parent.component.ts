import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NavigationService } from 'src/app/core/services/navigation.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, OnDestroy{
  currentUrl: any;
  routerEventSub: Subscription;

  constructor(private navigation: NavigationService) {
    this.routerEventSub = navigation.route$.subscribe(res =>{
      this.currentUrl = res;
    })
   }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.routerEventSub.unsubscribe();
  }

}

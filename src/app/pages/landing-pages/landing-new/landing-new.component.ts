import { Component, OnInit, } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { User } from 'src/app/core/models';
import { ApiService } from 'src/app/core/services/api.service.service';
import { AuthsService } from 'src/app/core/services/auths.service';
import { DataService } from 'src/app/core/services/dataservice.service ';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-landing-new',
  templateUrl: './landing-new.component.html',
  styleUrls: ['./landing-new.component.css']
})
export class LandingNewComponent implements OnInit {
  defaultImage = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  baseUrl = environment.baseUrl
  blogUrl = environment.blogsUrl;
  loggedIn: boolean;
  title = 'Best Activities and Programs for Kids in Jersey City - Wondrfly';
  categoryResponse: any;
  hide: boolean = true;
  landingImageIndex: number;
  landingImages = ['assets/landing/header.jpg',
    'assets/landing/header1.jpg',
  ]
  isStarted = true
  user: User;
  firstName: string;
  constructor(
    private apiservice: ApiService,
    public dataservice: DataService,
    public auth: AuthsService,
    private titleService: Title,
    private metaTagService: Meta,
  ) { }
  ngOnInit() {
    this.metaService()
    window.scroll(0, 0);
  }
  metaService() {
    this.apiservice.getMetaServiceByPageName('landing').subscribe(res => {
      if (res.isSuccess) {
        if (res.data !== null) {
          this.titleService.setTitle(res.data.title);
          this.metaTagService.updateTag(
            { name: 'description', content: res.data.description }
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: res.data.keywords }
          );
        }
        else {
          this.titleService.setTitle(this.title);
          this.metaTagService.updateTag(
            { name: 'description', content: 'Looking for the best programs and activities for your kids? Wondrfly is the leading platform for parents to discover indoor and outdoor activities for kids ages 3-14 years.' },
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: 'Best Activities and Programs, activities near me for toddlers, fitness classes for kids, online music lessons, online art classes' }
          );
        }
      }
      else {
        this.titleService.setTitle(this.title);
        this.metaTagService.updateTag(
          { name: 'description', content: 'Looking for the best programs and activities for your kids? Wondrfly is the leading platform for parents to discover indoor and outdoor activities for kids ages 3-14 years.' },
        );
        this.metaTagService.addTag(
          { name: 'keywords', content: 'Best Activities and Programs, activities near me for toddlers, fitness classes for kids, online music lessons, online art classes' }
        );
      }
    })

  }

}

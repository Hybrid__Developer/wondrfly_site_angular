import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SuggestionComponent } from './suggestion/suggestion.component';

const routes: Routes = [
  { path: 'my-wondrfly', component: SuggestionComponent },
  // {path:'landing', component:LandingComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingPagesRoutingModule { }

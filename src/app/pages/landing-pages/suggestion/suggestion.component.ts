import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import axios from 'axios';
import { Category, Child, User } from 'src/app/core/models';
import { LocalStorageService } from 'src/app/core/services';
import { ApiService } from 'src/app/core/services/api.service.service';
import { AuthsService } from 'src/app/core/services/auths.service';
import { DataService } from 'src/app/core/services/dataservice.service ';
import * as FileSaver from 'file-saver';
import { FormControl } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { environment } from 'src/environments/environment.prod';
import { Subscription } from 'rxjs';
import { DragScrollComponent } from 'ngx-drag-scroll';
@Component({
  selector: 'parent-suggestion',
  templateUrl: './suggestion.component.html',
  styleUrls: ['./suggestion.component.css']
})
export class SuggestionComponent implements OnInit, OnDestroy {
  @ViewChild('widgetsContent') widgetsContent: ElementRef;
  rightDisabled: boolean = false;
  leftDisabled: boolean = false;
  defaultImage = 'https://miro.medium.com/max/441/1*9EBHIOzhE1XfMYoKz1JcsQ.gif';
  blogUrl = environment.blogsUrl;
  baseUrl = environment.baseUrl;
  currentDate = new Date()
  title = 'Best Activities and Programs for Kids in Jersey City - Wondrfly';
  categories: any = [];
  tags: any = [];
  blogs: any = []
  featuredBlogs: any = []
  printables: any = []
  tweetData: any = []
  tweetDataBlogPath: any = []
  filterData: any = {
    subcatId: '',
    categoryId: '',
    activityName: '',
    searchedCategoryKey: '',
    lat: '',
    lng: '',
    kidAge: '',
    kidName: '',
    childIntrests: []
  }
  resources: any;
  categoriesBySearch: any = new Category;
  providersBySearch: any = new User;
  currentUser: any;
  kids: Child[];
  isNewFeaturePopUp: boolean;
  blogByCategory: any;
  allData: any = [];
  searchMywondrfly = new FormControl();
  @ViewChild('search') searchElementRef: ElementRef;
  @ViewChild('circle', { read: DragScrollComponent }) ds: DragScrollComponent;
  lat: string
  lng: string
  resourcesType = 'do-together'
  cookiesData: string;
  categoryData: any;
  isResourceScroll: boolean;
  categories_and_tags: any = Category
  subscribeUser: Subscription;
  kidsName: any = []
  allMaxAge: any;
  allMinAge: any;
  pdfLink: any;
  pdfTitle: any;
  suggestionSubCategories: any[] = [];
  partyPrograms: any;
  constructor(private router: Router,
    private apiservice: ApiService,
    public dataservice: DataService,
    public auth: AuthsService,
    private titleService: Title,
    private metaTagService: Meta,
    private ngxLoader: NgxUiLoaderService,
    private store: LocalStorageService,
  ) {
    this.currentUser = auth.userValue;
    this.subscribeUser = this.auth.userData.subscribe(res => {
      this.currentUser = res;
    })
    this.getChildByParentId(this.currentUser.id);
    if (!/\s/.test(this.currentUser.firstName)) {
      this.currentUser.firstName = this.currentUser.firstName + ' ' //added space for showing 1st name
    }
    if (!this.currentUser) {
      this.router.navigate(['']);
    }
  }

  scrollLeft(div) {
    document.getElementById('widgetsContent' + div).scrollLeft -= 650;
    // this.checkScroll()
  }

  scrollRight(div) {
    document.getElementById('widgetsContent' + div).scrollLeft += 650;
    // this.checkScroll()
  }

  searchSubCategory(key) {
    let groupDataAll: any = [
      { label: 'Keywords', data: [] },
      // { label: 'Provider', data: [] },
    ]
    if (!key) {
      this.allData = [];
    } else {
      this.apiservice.searchKeywords(key).subscribe((res: any) => {
        this.categoriesBySearch = res.data;
        res.data.map(keyword => { keyword.name = keyword.keywordName })
        groupDataAll[0].data = this.categoriesBySearch;
        this.allData = groupDataAll
      });
      // this.apiservice.searchUsers(key, "provider").subscribe((res: any) => {
      //     this.providersBySearch = res.data;
      //     this.providersBySearch = this.providersBySearch.filter(e => e.isActivated);
      //     var i;
      //     for (i = 0; i < this.providersBySearch.length; i++) {
      //       this.providersBySearch[i].name = this.providersBySearch[i]['firstName'];
      //       groupDataAll[1].data = this.providersBySearch;
      //       // this.allData = groupDataAll
      //     }
      //     this.allData = groupDataAll
      // });
    }
  }
  searchActivityByNameDate() {
    this.filterData.searchedCategoryKey = this.filterData.activityName
    this.filterData.kidAge = ''
    this.filterData.categoryId = ''
    this.filterData.lat = ''
    this.filterData.lng = ''
    this.filterData.childIntrests = []
    this.dataservice.setOption(this.filterData)
    this.router.navigate(['/search']);
  }
  searchBySubCategory(data, kid_id, kid_age) {
    this.transferValue(data.name)
    let id = data._id ? data._id : data.id;
    let filter = ``
    if (this.kids?.length) {
      // filter = `tagsIds=${id}`
      if (kid_id !== '' && kid_age !== '') {
        filter = `tagsIds=${id}&ageYear=${kid_age}`
        this.router.navigate(['/search'], {
          queryParams: {
            filter: filter,
            kid_id: kid_id,
            // kid_age: kid_age
          }
        });
      }
      else {
        filter = `tagsIds=${id}`
        this.router.navigate(['/search'], {
          queryParams: {
            filter: filter,
          }
        });
      }
    } else {
      filter = `tagsIds=${id}`
      this.router.navigate(['/search'], {
        queryParams: {
          filter: filter
        }
      });
    }
  }
  clickOnViewAllChildIntrests(indx, kid_id, kid_age) {
    // this.filterData.activityName = ''
    // this.filterData.categoryId = ''
    // this.filterData.lat = ''
    // this.filterData.lng = ''
    // this.filterData.subcatId = ''
    // this.filterData.childIntrests = []
    let filter = ``
    // this.kids[indx].interestInfo.forEach(intrest => {
    //   {
    //     this.filterData.childIntrests.push(intrest._id)
    //   }
    // });
    this.kids[indx].interestInfo.filter((intrest) => {
      if (intrest.programCount > 0) this.filterData.childIntrests.push(intrest._id)
    }
    );
    filter = `ageYear=${kid_age}&tagsIds=${this.filterData.childIntrests.toString()}`
    this.router.navigate(['/search'], {
      queryParams: {
        filter: filter,
        kid_id: kid_id,
        // kid_age:kid_age
      }
    });
    // this.dataservice.setOption(this.filterData)
    // this.router.navigate(['/search']);
  }
  searchByCategory(data) {
    // this.filterData.kidAge = ''
    // this.filterData.subcatId = ''
    // this.filterData.activityName = ''
    // this.filterData.categoryId = id
    // this.filterData.childIntrests = []
    // this.dataservice.setOption(this.filterData)
    // this.router.navigate(['/search']);
    let filter = `categoryId=${data._id}`
    this.router.navigate(['/search'], {
      queryParams: {
        filter: filter
      }
    });
  }

  doTogather(data) {
    var name = data.categoryName;
    name = name.toLowerCase();
    name = name.replace(/ /g, "-");
    name = name.replace(/\?/g, "-");
    this.router.navigate(['blogs/category/', name, data.id])
  }

  tweetCategory() {
    let data = {
      id: 14,
      categoryName: 'Funny Tweets',
    }
    var name = data.categoryName;
    name = name.toLowerCase();
    name = name.replace(/ /g, "-");
    name = name.replace(/\?/g, "-");
    this.router.navigate(['blogs/category/', name, data.id])
  }



  getCategoryList() {
    this.apiservice.getCategory().subscribe((res: any) => {
      this.categories = res;
      this.categories = this.categories.filter((item) => item.isActivated === true);
      this.categories = this.categories.map(item => {
        item.isCategory = true
        return item
      })
    });
    this.getTags()
  }

  downloadFile(file) {
    FileSaver.saveAs(this.blogUrl + file.url, file.name);
  }

  getTweet() {
    const responcee = axios.get(`${this.blogUrl}/funny-tweets?_sort=published_at:DESC&_limit=4`).then((response) => {
      this.tweetData = response.data;
      this.tweetData.forEach(element => {
        this.tweetDataBlogPath.push(new URL(element.blogLink).pathname)
      });
    });
  }

  // ------------------------------------------------get printables data  -------------------------------------------

  getPrintables() {
    const responcee = axios.get(`${this.blogUrl}/printables?_sort=published_at:DESC&_limit=4`).then((response) => {
      this.printables = response.data;
    });
  }

  // ------------------------------------------------get blogs  -------------------------------------------

  getBlog() {
    const responcee = axios.get(`${this.blogUrl}/blogs?_sort=published_at:DESC&_limit=4`).then((response) => {
      this.blogs = response.data;
    });
  }

  // ------------------------------------------------get featured blogs  -------------------------------------------

  getfeaturedBlog() {
    const responcee = axios.get(`${this.blogUrl}/blogs?_sort=published_at:DESC&isFeatured=true&_limit=4`).then((response) => {
      this.featuredBlogs = response.data;
    });
  }

  getBlogByCat() {
    const responcee = axios.get(`${this.blogUrl}/categories/?id=12`).then(response => {
      this.blogByCategory = response.data[0];
      this.blogByCategory.blogs.reverse();
    });
  };

  // setBlog(data) {
  //   var title = data.title
  //   title = title.toLowerCase();
  //   title = title.replace(/ /g,"-");
  //   title = title.replace(/\?/g,"-");
  //   this.router.navigate(['blogs/',title, data.id])
  // }

  providerSearch(key) {
    this.apiservice.searchUsers(key, 'provider').subscribe((res: any) => {
      if (res.data) {
        this.providersBySearch = res.data;
      }
      else {
        this.providersBySearch = []
      }
    })
  };

  goToProviderProfile(provider) {
    this.filterData.activityName = ''
    var providerName = provider.firstName;
    providerName = providerName.toLowerCase();
    providerName = providerName.replace(/ /g, "-");
    providerName = providerName.replace(/\?/g, "-");
    this.router.navigate(['/program/provider', providerName, provider._id]);
  };

  goToProgramDetail(data) {
    var programName = data.name;
    programName = programName.toLowerCase();
    programName = programName.replace(/ /g, "-");
    programName = programName.replace(/\?/g, "-");
    programName = programName.replace(/\//g, "-");

    let url = ``
    // if (Object.keys(this.filterObj).length) {
    //   const filter = new URLSearchParams(this.filterObj).toString();
    //   url = `/program/${programName}/${data._id}/${filter}`
    //   return url
    // }
    // else {
    url = `/program/${programName}/${data._id}/filter`
    return url
    // }
  }

  getChildByParentId(id) {
    this.ngxLoader.start();
    this.apiservice.getActiveChildByParent(id).subscribe((res: any) => {
      if (res?.isSuccess) {
        let kids = res.data;
        this.kids = kids.filter(item => item.interestInfo.length)
        this.kidsName = kids.map(item => item.name)
        this.allMinAge = Math.min.apply(Math, this.kids.map(function (kid) { return kid.age; }))
        this.allMaxAge = Math.max.apply(Math, this.kids.map(function (kid) { return kid.age; }))
      }
      else {
        this.ngxLoader.stop();
      }
      this.ngxLoader.stop();

    })
  }
  sendInvite() {
    this.store.setItem('sendInvite', '1')
    this.router.navigate(['/parent/profile', this.currentUser.id]);
  }
  // getForms(){
  //   this.typeFormService.getForms().subscribe((res: any) => {
  //   });
  // }

  setBlog(data) {
    var title = data.title;
    title = title.toLowerCase();
    title = title.replace(/ /g, "-");
    title = title.replace(/\?/g, "-");
    const url = this.router.serializeUrl(
      this.router.createUrlTree(['blogs/', title, data.id])
    );
    window.open(url, '_blank');
  }

  setVisit() {
    // this.cookiesData = this.cookies.get('isTour');
  }
  onTab(e, value) {
    if (this.allData[0].data.length) {
      this.searchMywondrfly.setValue(value)
    }
  }
  ngOnInit() {
    this.searchMywondrfly.valueChanges.subscribe((value) => {
      if (value) { this.searchSubCategory(value) } else {
        this.allData = [];
      }
    })
    this.metaService()
    this.setVisit();
    this.getTweet();
    this.getPrintables();
    this.getSuggestionSubCat();
    this.getProgramByType();
    this.getBlogByCat();
    this.getfeaturedBlog();
    this.getBlog();
    this.getCategoryList();
    // this.mapsAPILoader.load().then(() => {
    //   this.geoCoder = new google.maps.Geocoder;
    //   let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
    //   autocomplete.addListener('place_changed', () => {
    //     this.ngZone.run(() => {
    //       let place: google.maps.places.PlaceResult = autocomplete.getPlace();
    //       if (place.geometry === undefined || place.geometry === null) {
    //         return;
    //       }

    //       // set latitude, longitude
    //       this.lat = String(place.geometry.location.lat());
    //       this.lng = String(place.geometry.location.lng());
    //     });
    //   });
    // });
  }
  metaService() {
    this.apiservice.getMetaServiceByPageName('my-wondrfly').subscribe(res => {
      if (res.isSuccess) {
        if (res.data !== null) {
          this.titleService.setTitle(res.data.title);
          this.metaTagService.updateTag(
            { name: 'description', content: res.data.description }
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: res.data.keywords }
          );
        }
        else {
          this.titleService.setTitle(this.title);
          this.metaTagService.updateTag(
            { name: 'description', content: 'Looking for the best programs and activities for your kids? Wondrfly is the leading platform for parents to discover indoor and outdoor activities for kids ages 3-14 years.' },
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: 'Best Activities and Programs, activities near me for toddlers, fitness classes for kids, online music lessons, online art classes' }
          );
        }
      }
      else {
        this.titleService.setTitle(this.title);
        this.metaTagService.updateTag(
          { name: 'description', content: 'Looking for the best programs and activities for your kids? Wondrfly is the leading platform for parents to discover indoor and outdoor activities for kids ages 3-14 years.' },
        );
        this.metaTagService.addTag(
          { name: 'keywords', content: 'Best Activities and Programs, activities near me for toddlers, fitness classes for kids, online music lessons, online art classes' }
        );
      }
    })

  }
  selectSearchedOption(data) {
    this.dataservice.changeInput(data.name)
    if (data.role == 'provider') {
      this.filterData.activityName = "";
      data.name = data.name.toLowerCase();
      data.name = data.name.replace(/ /g, "-");
      data.name = data.name.replace(/\?/g, "-");
      this.router.navigate(["/program/provider", data.name, data._id])
    }
    else {
      let filter = ``
      this.addKeywordPopularity(data._id)
      switch (data.keywordType) {
        case 'category':
          filter = `categoryId=${data.keywordValue[0].category.toString()}`
          break;
        case 'subCategory':
          if (data.keywordValue[0].category.length) {
            filter = `categoryId=${data.keywordValue[0].category.toString()}&tagsIds=${data.keywordValue[0].subcategory.toString()}`
          }
          else {
            filter = `tagsIds=${data.keywordValue[0].subcategory.toString()}`

          }
          break;
        case 'age':
          filter = `ageFrom=${data.keywordValue[0].from}&ageTo=${data.keywordValue[0].to}`
          break;
        case 'price':
          filter = `priceFrom=${data.keywordValue[0].from}&priceTo=${data.keywordValue[0].to}`
          break;
        case 'dates':
          filter = `fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}`
          break;
        case 'type':
          filter = `type=${data.keywordValue[0].type.toString()}`
          break;
        case 'time':
          filter = `time=${data.keywordValue[0].time.toString()}`
          break;
        case 'days':
          filter = `day=${data.keywordValue[0].days.toString()}`
          break;
        case 'format':
          filter = `inpersonOrVirtual=${data.keywordValue[0].format.toString()}`
          break;
        case 'TopRated':
          filter = `ratingFrom=${data.keywordValue[0].from}&ratingTo=${data.keywordValue[0].to}`

          break;
        case 'isChildDropOff':
          filter += `isChildCare=${data.keywordValue[0].isChildDropOff}`
          break;
        case 'indoorOutdoor':
          filter += `indoorOroutdoor=${data.keywordValue[0].indoorOutdoor}`
          break;

      }
      // this.addKeywordPopolarity()
      this.router.navigate(['/search'], {
        queryParams: {
          filter: filter
        }
      })
    }

  }
  freePrograms() {
    this.router.navigate(['search'], {
      queryParams: {
        filter: `isFree=true`
      }
    })
  }
  addKeywordPopularity(id) {
    this.apiservice.addKeywordPopularity(id).subscribe((res: any) => {
    })
  }
  transferValue(value) {
    this.dataservice.changeInput(value)
  }
  searchKeyword(txt, extraField?) {
    this.transferValue(txt)
    if (txt?.length) {
      this.apiservice.searchMultipleKeywords(txt).subscribe((res: any) => {
        const uniqueArry: any = [...new Map(res.data.map((item) => [item["keywordName" && "keywordType"], item])).values()];
        if (uniqueArry) {
          let filter = ``
          for (let data of uniqueArry) {
            switch (data.keywordType) {
              case 'category':
                if (filter) {
                  filter += `&categoryId=${data.keywordValue[0].category.toString()}`
                } else {
                  filter += `categoryId=${data.keywordValue[0].category.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'subCategory':
                if (filter) {
                  if (data.keywordValue[0].category.length) {
                    filter += `&tagsIds=${data.keywordValue[0].subcategory.toString()}&categoryId=${data.keywordValue[0].category.toString()}`
                  }
                  else {
                    filter += `&tagsIds=${data.keywordValue[0].subcategory.toString()}`
                  }
                } else {
                  if (data.keywordValue[0].category.length) {
                    filter += `tagsIds=${data.keywordValue[0].subcategory.toString()}&categoryId=${data.keywordValue[0].category.toString()}`
                  }
                  else {
                    filter += `tagsIds=${data.keywordValue[0].subcategory.toString()}`
                  }
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'age':
                if (filter) {
                  filter += `&ageFrom=${data.keywordValue[0].from}&ageTo=${data.keywordValue[0].to}`
                } else {
                  filter += `ageFrom=${data.keywordValue[0].from}&ageTo=${data.keywordValue[0].to}`
                }
                break;
              case 'price':
                if (filter) {
                  filter += `&priceFrom=${data.keywordValue[0].from}&priceTo=${data.keywordValue[0].to}`
                } else {
                  filter += `priceFrom=${data.keywordValue[0].from}&priceTo=${data.keywordValue[0].to}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'dates':
                if (txt === 'Summer' && extraField) {
                  if (this.kids.length == 1) {
                    if (filter) {
                      filter = `&ageFrom=${this.allMaxAge - 1}&ageTo=${this.allMaxAge + 1}`
                    }
                    else {
                      filter = `ageFrom=${this.allMaxAge - 1}&ageTo=${this.allMaxAge + 1}`
                    }
                  }
                  else {
                    if (filter) {
                      filter = `&ageFrom=${this.allMinAge}&ageTo=${this.allMaxAge}`
                    }
                    else {
                      filter = `ageFrom=${this.allMinAge}&ageTo=${this.allMaxAge}`
                    }
                  }
                  let type = 'categoryId'
                  if (extraField.isTag) {
                    type = 'tagsIds'
                  }
                  if (filter) {
                    filter += `&fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}&${type}=${extraField.isCategory ? extraField.id : extraField._id}&type=Camps`
                  } else {
                    filter += `fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}&${type}=${extraField.isCategory ? extraField.id : extraField._id}&type=Camps`
                  }
                } else {
                  if (filter) {
                    filter += `&fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}`
                  } else {
                    filter += `fromDate=${data.keywordValue[0].from}&toDate=${data.keywordValue[0].to}`
                  }
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'type':
                data.keywordValue[0].type = data.keywordValue[0].type.map(function (el) {
                  return el.trim();
                });
                if (filter) {
                  filter += `&type=${data.keywordValue[0].type.toString()}`
                } else {
                  filter += `type=${data.keywordValue[0].type.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'time':
                if (extraField === 'day') {
                  if (filter) {
                    filter += `&time=${data.keywordValue[0].time.toString()}&day=tuesday`
                  } else {
                    filter += `time=${data.keywordValue[0].time.toString()}&day=tuesday`
                  }
                }
                else {
                  if (filter) {
                    filter += `&time=${data.keywordValue[0].time.toString()}`
                  } else {
                    filter += `time=${data.keywordValue[0].time.toString()}`
                  }
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'days':
                if (filter) {
                  filter += `&day=${data.keywordValue[0].days.toString()}`
                } else {
                  filter += `day=${data.keywordValue[0].days.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'format':
                if (filter) {
                  filter += `&inpersonOrVirtual=${data.keywordValue[0].format.toString()}`
                } else {
                  filter += `inpersonOrVirtual=${data.keywordValue[0].format.toString()}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'topRated':
                if (filter) {
                  filter += `&ratingFrom=${data.keywordValue[0].from}&ratingTo=${data.keywordValue[0].to}`
                } else {
                  filter += `ratingFrom=${data.keywordValue[0].from}&ratingTo=${data.keywordValue[0].to}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'isChildDropOff':
                if (filter) {
                  filter += `&isChildCare=${data.keywordValue[0].isChildDropOff}`
                } else {
                  filter += `isChildCare=${data.keywordValue[0].isChildDropOff}`
                }
                this.addKeywordPopularity(data._id)
                break;
              case 'indoorOutdoor':
                if (filter) {
                  filter += `&indoorOroutdoor=${data.keywordValue[0].indoorOutdoor}`
                } else {
                  filter += `indoorOroutdoor=${data.keywordValue[0].indoorOutdoor}`
                }
                this.addKeywordPopularity(data._id)
                break;

            }
          }
          if (filter) {
            this.router.navigate(['search'], {
              queryParams: {
                filter: filter
              }
            })
          }
          else {
            this.router.navigate(['search'], {
              queryParams: {
                filter: `keyword=${txt}`
              }
            })
          }

        } else {
          this.router.navigate(['/search'], {
            queryParams: {
              filter: `keyword=${txt}`
            }
          })
        }
      })
    }
    else {
      this.router.navigate(['/search'])
    }
  }
  previewPDF(printable) {
    // window.open(this.blogUrl + url, '_blank');
    this.pdfLink = this.blogUrl + printable.file?.url;
    this.pdfTitle = printable.title;
    document.getElementById("openModalButton").click();

  }

  getSuggestionSubCat() {
    this.ngxLoader.start();
    this.apiservice.getSuggestion(this.currentUser.id).subscribe((res: any) => {
      if (res.isSuccess) {
        this.suggestionSubCategories = res.data;
      }
    })
  }

  getProgramByType() {
    this.ngxLoader.start();
    this.apiservice.getProgramByType(1, 4, "party").subscribe((res: any) => {
      if (res.isSuccess) {
        this.partyPrograms = res.items;
      }
    })
  }

  getTags() {
    this.apiservice.getTag().subscribe((res: any) => {
      this.tags = res.data.filter(res => res.isActivated)
      this.tags = this.tags.map(tag => {
        tag.isTag = true
        return tag
      })
      this.tags = [...this.tags, ...this.categories]
      this.tags.map(item => {
        switch (item.name) {
          case 'Soccer':
            this.tags[0] = item;
            break;
          case 'Swimming':
            this.tags[1] = item;
            break;
          case 'Tennis':
            this.tags[2] = item;
            break;
          case 'Arts & Crafts':
            this.tags[3] = item;
            break;
          case 'Coding':
            this.tags[4] = item;
            break;
          case 'Music':
            this.tags[5] = item;
            break;
          case 'Dance':
            this.tags[6] = item;
            break;
          case 'Gymnastics':
            this.tags[7] = item;
            break;
        }
        return item
      })
    })
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  ngOnDestroy(): void {
    this.subscribeUser.unsubscribe();
  }
}

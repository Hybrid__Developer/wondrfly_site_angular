import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingPagesRoutingModule } from './landing-pages-routing.module';
import { LandingComponent } from './landing/landing.component';
import { CoreModule } from 'src/app/core/core.module';
import { MailchimpSubscribeFormModule } from 'src/app/core/shared/shared-components/mailchimp-subscribe-form/mailchimp-subscribe-form.module';
import { NgxTweetModule } from 'ngx-tweet';
import { ReactiveFormsModule } from '@angular/forms';
import { SuggestionComponent } from './suggestion/suggestion.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [LandingComponent, SuggestionComponent],
  imports: [
    CommonModule,
    LandingPagesRoutingModule,
    CoreModule,
    ReactiveFormsModule,
    MailchimpSubscribeFormModule,
    NgxTweetModule,
    DragScrollModule,
    PdfViewerModule
  ]
})
export class LandingPagesModule { }

import { Component } from '@angular/core';
import { AuthsService } from 'src/app/core/services/auths.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent {
  isLogin = false;
  userData: any = {};

  constructor(private auths: AuthsService) {
    this.userData = auths.userValue;
  }
}

import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-parent-landing',
  templateUrl: './parent-landing.component.html',
  styleUrls: ['./parent-landing.component.css']
})
export class ParentLandingComponent implements OnInit {
  @Output()
  showHide = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  clickStarted() {
    this.showHide.emit();
  }
}

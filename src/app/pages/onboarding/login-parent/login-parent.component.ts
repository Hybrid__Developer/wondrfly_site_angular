import { Component, OnInit, NgZone, ElementRef, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api.service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Child, User } from 'src/app/core/models';
import * as moment from 'moment';
import { MapsAPILoader } from '@agm/core';
import { Meta, Title } from '@angular/platform-browser';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { MapTheme } from 'src/app/core/common/map-theme';
import { AuthsService } from 'src/app/core/services/auths.service';
import { Subscription } from 'rxjs';
import { DragScrollComponent } from 'ngx-drag-scroll';
@Component({
  selector: 'app-login-parent',
  templateUrl: './login-parent.component.html',
  styleUrls: ['./login-parent.component.css']
})
export class LoginParentComponent implements OnInit, OnDestroy {
  @ViewChild('catArray', { read: DragScrollComponent }) ds: DragScrollComponent;
  currentYear = new Date().getFullYear()
  addChildForm: FormGroup;
  locationForm: FormGroup;
  setpasswordForm: FormGroup;
  keyword = '';
  addChildData: any = new Child
  categoryIds = [];
  parent: User;
  firstName: string;
  baseUrl = environment.baseUrl
  // ---------------autucomplete-------------
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  tags: any = [];
  message: string = 'child added Successfully';
  categories: any = [];
  filtredCats: any = []
  interests: any = [];
  kid = new Child;
  tempKid: Child;
  kids: Child[] = [];
  step0 = true;
  step1 = false;
  step2 = false;
  step3 = false;
  step4 = false;
  step5 = false;
  // step5 = false
  total = 6;
  finished = 1
  title = 'Onboarding - Wondrfly';
  markerUrl = 'assets/location.svg';
  private geoCoder;
  progressBarVaue = 20;
  zoom: number = 14;
  latitude: Number = 40.72652470735903;
  parentEmailOrName: string;
  longitude: Number = -74.05900394007715;
  @ViewChild('search', { static: false }) searchElementRef: ElementRef;
  searchTags: any = [];
  searchesCatg: any = [];
  selectedTags: any = [];
  removedTag: any;
  searchedTags: any = []
  subCategoryCheckbox: any = []
  categoryChecked: any = []
  searchTagValue = new FormControl()
  subscribeUser: Subscription;
  hidePassword: boolean = true;
  isAddAnother: boolean = false;
  kidName: any;
  kidMonth: any;
  kidYear: any;
  selectedCatId: any
  location: string;
  kid_Name:any=[]
  constructor(private router: Router,
    private apiservice: ApiService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private titleService: Title,
    private metaTagService: Meta,
    private toastr: ToastrService,
    public mapTheme: MapTheme,
    private auth: AuthsService,
  ) {
    this.parent = this.auth.userValue;
    this.getParent();
    this.subscribeUser = this.auth.userData.subscribe(res => {
      this.parent = res;
      this.getParent();
    });
  }

  ngOnInit() {
    this.dateV();
    this.metaService()
    window.scroll(0, 0);
    this.getCategoryList();
    // this.getTagList();
    this.locationForm = new FormGroup({
      location: new FormControl('', [Validators.required]),
      addressLine1: new FormControl('', [Validators.required]),
      lat: new FormControl('',),
      lng: new FormControl('',),

    });
    this.setpasswordForm = new FormGroup({
      password: new FormControl('', this.parent?.onBoardingCount?.count > 3 ? [Validators.required] : []),
      userName: new FormControl('', this.parent?.onBoardingCount?.count > 3 ? [Validators.required] : []),
      email: new FormControl('', this.parent?.onBoardingCount?.count > 3 ? [Validators.required] : []),
    });
    this.addChildForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      dob: new FormControl('', []),
      month: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required]),
    });
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder;
      var options = {
        // types: ['(cities)'],
        componentRestrictions: { country: "us" }
      };
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, options);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          this.locationForm.value.location = place.formatted_address;
          this.locationForm.value.addressLine1 = this.locationForm.value.location
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.zoom = 12;
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.parent.lat = String(place.geometry.location.lat());
          this.parent.lng = String(place.geometry.location.lng());
        });
      });
    });
  }

  moveLeft() {
    this.ds.moveLeft();
  }

  moveRight() {
    this.ds.moveRight();
  }

  getParent() {
    if (this.parent != null) {
      this.firstName = this.parent?.firstName.split(' ')[0];
      this.location = this.parent.location;
      this.getChildByparent();
      this.parentEmailOrName = this.parent.email ? this.parent.email : this.parent.userName;
      if (this.parent.isOnBoardingDone) {
        // this.step0 = false;
        // this.step1 = false;
        // this.step2 = false;
        // this.step3 = false;
        // this.step4 = false;
        // this.step5 = true;
        // this.finished = 6;
      }
    }
  }

  logo() {
    this.router.navigate(['/search']);
  }

  remove(kidIndx, TagIndx) {
    if (this.kids[kidIndx].interestInfo[TagIndx] !== -1) {
      this.kids[kidIndx]?.interestInfo.splice(TagIndx, 1);
      this.updateChild(this.kids[kidIndx], true)
    }
  }

  @HostListener('document:click', ['$event']) clickedOutside($event) {
    // here you can hide your review popup
    this.keyword = ''
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.addChildData.interestInfo.push(event.option.value);

  }

  openSearch() {
    this.router.navigate(['/search']);
  }

  dateV() {
    let today = new Date()
    let maxDate = today.getFullYear()
    // document.getElementById("m").setAttribute("min", '' + 1);
    // document.getElementById("m").setAttribute("max", '' + 12);
    document.getElementById("y").setAttribute("max", '' + maxDate);
    let eighteenYearsAgo = new Date(today.setFullYear(today.getFullYear() - 18)).getFullYear();
    document.getElementById("y").setAttribute("min", '' + eighteenYearsAgo);
  }
  validAge2() {
    this.kid.name = this.kidName;

    var dob = new Date(this.addChildForm.value.year, this.addChildForm.value.month, 0);
    this.kid.dob = '' + dob
    let thisYear = new Date().getFullYear()
    let today = new Date()
    let fifteenYearsAgo = new Date(today.setFullYear(today.getFullYear() - 15)).getFullYear();
    var birth = new Date(this.kid.dob);
    let birthYear = moment(birth).format('YYYY');
    let currentYear = moment(Date.now()).format('YYYY');
    var d1 = new Date();
    var d2 = new Date(this.kid.dob);
    if (this.addChildForm.value.year >= fifteenYearsAgo && this.addChildForm.value.year > currentYear){
      this.toastr.warning('Invalid  Birth Year!')
    }
    else if(d2.getTime() >= d1.getTime()){
      this.toastr.warning('please fill valid  Birth Year')
    }
    else if (birthYear >= currentYear) {
      this.toastr.warning('Please Fill Valid Birth Year!')
    }
    else {
      var ageDifMs = Date.now() - birth.getTime();
      var ageDate = new Date(ageDifMs); // miliseconds from epoch
      var age = Math.abs(ageDate.getUTCFullYear() - 1970);
      if (age > 18) {
        this.toastr.warning("Child age should be 18 years or less!");
      } else {
        this.kid.age = String(age);
        this.kid.relationToChild = 'father'
        this.kid.sex = 'male'
        this.kid.parentId = this.parent?.id
        this.nextStep()
      }
    }
  }
  // validAge() {
  //   this.kid.name = this.kidName;
  //   var dob = new Date(this.addChildForm.value.year, this.addChildForm.value.month, 0);

  //   this.kid.dob = '' + dob
  //   let thisYear = new Date().getFullYear()
  //   let today = new Date()
  //   let fifteenYearsAgo = new Date(today.setFullYear(today.getFullYear() - 15)).getFullYear();
  //   if (this.addChildForm.value.year <= thisYear && this.addChildForm.value.year >= fifteenYearsAgo && this.addChildForm.value.month <= 12 && this.addChildForm.value.month >= 1) {
  //     var birth = new Date(this.kid.dob);
  //     let birthYear = moment(birth).format('YYYY');
  //     let currentYear = moment(Date.now()).format('YYYY');
  //     var d1 = new Date();
  //     var d2 = new Date(this.kid.dob);
  //     if (!this.kid.name) {
  //       this.toastr.warning('please fill valid  name')
  //     }
  //     else if (d2.getTime() >= d1.getTime()) {
  //       this.toastr.warning('please fill valid DOB')
  //     }
  //     else if (birthYear >= currentYear) {
  //       this.toastr.warning('Please Fill Valid Birth Year!')
  //     }
  //     else {
  //       var ageDifMs = Date.now() - birth.getTime();
  //       var ageDate = new Date(ageDifMs); // miliseconds from epoch
  //       var age = Math.abs(ageDate.getUTCFullYear() - 1970);
  //       if (age > 18) {
  //         this.toastr.warning("Child age should be 18 years or less");
  //       } else {
  //         this.kid.age = String(age);
  //         this.kid.relationToChild = 'father'
  //         this.kid.sex = 'male'
  //         this.kid.parentId = this.parent?.id
  //         this.nextStep()
  //       }
  //     }
  //   }
  //   else {
  //     this.toastr.warning('please fill valid DOB')
  //   }
  // }
  addMultipleChild() {
    let body = {
      parentId: this.parent.id,
      children: this.kids
    }
    this.apiservice.addMultipleChild(body).subscribe((res: any) => {
    });
  }

  getChildByparent() {
    this.apiservice.getActiveChildByParent(this.parent.id).subscribe((res: any) => {
      if (res.isSuccess) {
        this.kids = res.data
      } else if (res.error == "child Not Found") {
        this.kids = []
      }
    })
  }

  addChild() {
    // this.kid.interestInfo = this.selectedTags
    // if (this.parent.id && this.kids.length) {
    //   let i = 0
    //   let index = this.kids.findIndex(x => x === this.kid)
    //   if (index == -1) {
    //     this.kids.push(this.kid)
    //   }
    //   for (let kid of this.kids) {
    //     i++;
    this.apiservice.addChild(this.kid).subscribe((res: any) => {
      if (res.isSuccess) {
        this.kid = new Child();
        this.getChildByparent();
        this.nextStep();
      }
    });
    //   }
    //   this.updateParent()
    // }
    // else {
    //   this.updateParent()
    // }
  }

  deleteKidById(kid) {
    let id = kid.id ? kid.id : kid._id;
    this.apiservice.deleteChild(id).subscribe((res: any) => {
      if (res.isSuccess) {
        this.getChildByparent();
        if (this.kids.length < 1) {
          this.kid = new Child();
          this.kidName = "";
          this.step4 = false;
          this.step2 = true;
        }
      }
    })
  }

  addUpdateChild() {
    let id = this.kid.id ? this.kid.id : this.kid._id;
    if (id) {
      this.updateChild(this.kid);
    } else {
      this.addChild();
    }
  }

  updateChild(kid, nextBlock?) {
    let kidId = kid.id ? kid.id : kid._id;
    this.apiservice.updateChild(kidId, kid).subscribe((res: any) => {
      if (res.isSuccess) {
        this.getChildByparent();
        if (nextBlock) {
          return
        }
        this.nextStep();
      }
    })
  }

  addChildTemp() {
    this.nextStep();
    let index = this.kids.findIndex(x => x === this.kid)
    if (index == -1) {
      this.kids.push(this.kid);
    }
  }

  deleteKid(kid) {
    const index: number = this.kids.indexOf(kid);
    if (index !== -1) {
      this.kids.splice(index, 1);
      if (!this.kids.length) {
        this.addChildForm.reset();
        this.kid = new Child();
        this.step1 = false;
        this.step2 = true;
        this.step3 = false;
        this.step4 = false;
        this.step5 = false;
        this.finished = 3;
      }
    }
  }

  addAnotherButton() {
    this.tempKid = this.kid;
    this.isAddAnother = true;
    this.addChildForm.reset();
    this.kid = new Child();
    this.selectedCategory(this.categories[0])
  }

  cancleAnotherKid() {
    this.kid = this.tempKid;
    this.isAddAnother = false
  }

  editKid(kid: Child) {
    this.kidYear = new Date(kid.dob).getFullYear();
    this.kidMonth = new Date(kid.dob).getMonth() + 1;
    this.selectedCategory(this.categories[0])
    this.kid = kid;
    this.step0 = false;
    this.step1 = false;
    this.step2 = true;
    this.step3 = false;
    this.step4 = false;
    this.step5 = false;
    this.kidName = kid.name;
  }

  validateEmail(emailAdress) {
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (emailAdress.match(regexEmail)) {
      return true;
    } else {
      return false;
    }
  }
  // ------------------------------------------auto-complete search functionality for tags-------------------
  selectEvent(item) {
    if (this.step4 === true || this.step3 === true) {
      const index: number = this.selectedTags.indexOf(item);
      if (this.selectedTags.indexOf(item) === -1 && this.interests.length <= 4) {
        this.selectedTags.push(item);
        item.active = !item.active;
      }
      else if (index !== -1) {
        item.active = !item.active;
        this.selectedTags.splice(index, 1);
      }
    }
  }

  matchCategory(id) {
    let index = this.filtredCats.findIndex(x => x._id == id)
    if (index !== -1) {
      return this.filtredCats[index].name
    }
  }

  removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject = {};

    for (var i in originalArray) {
      lookupObject[originalArray[i][prop]] = originalArray[i];
    }
    for (i in lookupObject) {
      newArray.push(lookupObject[i]);
    }
    return newArray;
  }

  // ------------------------------------------get categories-----------------------------------------------------------------
  getCategoryList() {
    this.apiservice.getAllCategory().subscribe((res: any) => {
      this.categories = res;
      this.categories = this.categories.filter((item) => item.isActivated === true);
      this.selectedCategory(this.categories[0])
    });
  }

  selectDeselectTags(tag, tagIndx) {
    const index = this.kid.interestInfo.indexOf(tag);
    if (index >= 0) {
      this.kid.interestInfo.splice(index, 1);
      this.tags[tagIndx].selected = false;
    }
    else {
      this.kid.interestInfo.push(tag)
      this.tags[tagIndx].selected = true
    }
  }
  kidsTagCheckId(tag) {
    // const index = this.kid.interestInfo.indexOf(tag);
    const index = this.kid.interestInfo.findIndex(x => x._id === tag._id);
    if (index >= 0) {
      return true;
    }
    return false
  }

  metaService() {
    this.apiservice.getMetaServiceByPageName('parent-onboarding').subscribe(res => {
      if (res.isSuccess) {
        if (res.data !== null) {
          this.titleService.setTitle(res.data.title);
          this.metaTagService.updateTag(
            { name: 'description', content: res.data.description }
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: res.data.keywords }
          );
        }
        else {
          this.titleService.setTitle(this.title);
          this.metaTagService.updateTag(
            { name: 'description', content: "Structured and well-planned onboarding process for parents to explore kids program, fun activities, and online classes. Visit Wondrfly's website for more info." }
          );
          this.metaTagService.addTag(
            { name: 'keywords', content: 'kids on boarding,onboarding' }
          );
        }
      }
      else {
        this.titleService.setTitle(this.title);
        this.metaTagService.updateTag(
          { name: 'description', content: "Structured and well-planned onboarding process for parents to explore kids program, fun activities, and online classes. Visit Wondrfly's website for more info." }
        );
        this.metaTagService.addTag(
          { name: 'keywords', content: 'kids on boarding,onboarding' }
        );
      }
    })
  }
  nextStep() {
    window.scroll(0, 0);
    if (this.step0) {
      this.step0 = false;
      this.step1 = true;
      this.progressBarVaue += 20;
      this.finished = 2
    }
    else if (this.step1) {
      if (this.kids.length > 0) {
        this.step1 = false;
        this.step4 = true;
        this.progressBarVaue += 20;
        this.finished = 5
      } else {
        this.step1 = false;
        this.step2 = true;
        this.progressBarVaue += 20;
        this.finished = 3
      }
    }
    else if (this.step2) {
      this.step3 = true;
      this.step2 = false;
      if (this.kids.length == 0) {
        this.progressBarVaue += 20;
        this.finished = 4
      }
    }
    else if (this.step3) {
      this.step4 = true;
      this.step3 = false;
      if (!this.kids.length) {
        this.progressBarVaue += 20;
        this.finished = 5
      }
      this.isAddAnother = false;
    }
    else if (this.step4) {
      if (!this.kid.interestInfo.length && this.kid.name) {
        this.step0 = false;
        this.step1 = false;
        this.step2 = false;
        this.step3 = true;
        this.step4 = false;
        this.step5 = false;
      } else {
        this.step5 = true;
        this.step4 = false;
        this.progressBarVaue += 20;
        this.finished = 6
      }
    }
  }
  backStep() {
    window.scroll(0, 0);
    if (this.step1) {
      this.step0 = true;
      this.step1 = false;
      this.progressBarVaue -= 20;
      this.finished = 1
    }
    else if (this.step2) {
      this.step1 = true;
      this.step2 = false;
      this.progressBarVaue -= 20;
      this.finished = 2
    }
    else if (this.step3) {
      if (this.isAddAnother) {
        this.step3 = false;
        this.step4 = true;
        this.isAddAnother = false;
        this.progressBarVaue -= 20;
        this.finished = 5;
      } else {
        this.step2 = true;
        this.step3 = false;
        this.progressBarVaue -= 20;
        this.finished = 3;
      }
    }
    else if (this.step4) {
      if (this.kids.length > 0) {
        this.step1 = true;
        this.step4 = false;
        this.progressBarVaue -= 20;
        this.finished = 1;
      } else {
        this.step3 = true;
        this.step4 = false;
        this.progressBarVaue -= 20;
        this.finished = 4;
      }
    }
    else if (this.step5) {
      this.step4 = true;
      this.step5 = false;
      this.progressBarVaue -= 20;
      this.finished = 5
    }
  }
  skip() {
    let parent = {
      onBoardingCount: { count: this.parent.onBoardingCount.count ? this.parent.onBoardingCount.count + 1 : 1 },
      email: ''
    }
    if (this.validateEmail(this.setpasswordForm.value.userName)) {
      parent.email = this.setpasswordForm.value.userName
    }
    this.apiservice.updateParent(this.parent.id, parent).subscribe((res: any) => {
      if (res.isSuccess) {
        window.location.pathname = '/my-wondrfly'
        this.auth.setUser(res.data);
        this.auth.getProfileProgress();
        this.router.navigate(['/my-wondrfly'])
      }
      else { this.toastr.info('Username already in use!') }
    });
  }
  // -----------------------------------------------update parent----------------------------------
  updateParent() {
    let parent = {
      addressLine1: this.locationForm.value.addressLine1 ? this.locationForm.value.addressLine1 : this.parent.addressLine1,
      location: this.locationForm.value.location ? this.locationForm.value.location : this.parent.location,
      lat: this.latitude,
      lng: this.longitude,
      // firstName: this.setpasswordForm.value.firstName,
      password: this.setpasswordForm.value.password,
      userName: this.setpasswordForm.value.userName,
      email: '',
      onBoardingCount: { count: this.parent.onBoardingCount.count ? this.parent.onBoardingCount.count + 1 : 1 },
    }
    if (this.validateEmail(this.setpasswordForm.value.userName)) {
      parent.email = this.setpasswordForm.value.userName
    }
    var passwordSetRequest: any = ''
    if (this.setpasswordForm.value.password || this.setpasswordForm.value.userName || this.setpasswordForm.value.email) {
      passwordSetRequest = `?passwordSetRequest=true`
    }
    this.apiservice.updateParent(this.parent.id, parent, passwordSetRequest).subscribe((res: any) => {
      if (res.isSuccess) {
        window.location.pathname = '/my-wondrfly'
        this.auth.setUser(res.data);
        this.auth.getProfileProgress();
        this.router.navigate(['/my-wondrfly'])
      }
      else {
        this.toastr.info('Username already in use!')
      }
    });
  }

  checkOrUncheckAllTags(e, categoryIndx) {
    if (e.target.checked === true) {
      if (!this.searchedTags[categoryIndx].tags.length) {
        this.apiservice.getTagByCategoryId(this.searchedTags[categoryIndx].category._id).subscribe((res: any) => {
          if (res.isSuccess) {
            this.searchedTags[categoryIndx].tags = res.data
            this.searchedTags[categoryIndx].category.isSelected = true;
            this.searchedTags[categoryIndx].tags.forEach(tag => {
              tag.isSelected = true
              if (this.kid.interestInfo.indexOf(tag) == -1) {
                if (!this.kid.interestInfo.find(category => category._id === tag._id)) {
                  this.kid.interestInfo.push(tag)
                }
              }
            });
          }
        })
      }
      else {
        this.searchedTags[categoryIndx].category.isSelected = true;
        this.searchedTags[categoryIndx].tags.forEach(tag => {
          tag.isSelected = true
          if (this.kid.interestInfo.indexOf(tag) == -1) {
            if (!this.kid.interestInfo.find(category => category._id === tag._id)) {
              this.kid.interestInfo.push(tag)
            }
          }
        });
      }
    } else {
      if (!this.searchedTags[categoryIndx].tags.length) {
        this.apiservice.getTagByCategoryId(this.searchedTags[categoryIndx].category._id).subscribe((res: any) => {
          if (res.isSuccess) {
            this.searchedTags[categoryIndx].tags = res.data
            this.searchedTags[categoryIndx].category.isSelected = false;
            this.searchedTags[categoryIndx].tags.forEach(tag => {
              let index = this.kid.interestInfo.findIndex(x => x._id === tag._id)
              if (index !== -1) {
                this.kid.interestInfo.splice(index, 1)
              }
              tag.isSelected = false
            });
          }
        })
      } else {
        this.searchedTags[categoryIndx].category.isSelected = false;
        this.searchedTags[categoryIndx].tags.forEach(tag => {
          let index = this.kid.interestInfo.findIndex(x => x._id === tag._id)
          if (index !== -1) {
            this.kid.interestInfo.splice(index, 1)
          }
          tag.isSelected = false
        });
      }
    }
  }
  checkOrUncheckTag(e, categoryIndx, tagIndex) {
    // const value = (element) => element === false;
    let unchecked = this.searchedTags[categoryIndx].tags.filter(tag => !tag.isSelected)
    if (e.target.checked === true) {
      if (this.kid.interestInfo.indexOf(this.searchedTags[categoryIndx].tags) == -1) {
        if (!this.kid.interestInfo.find(category => category._id === this.searchedTags[categoryIndx].tags[tagIndex]._id)) {
          this.kid.interestInfo.push(this.searchedTags[categoryIndx].tags[tagIndex])
        }
      }
      this.searchedTags[categoryIndx].tags[tagIndex].isSelected = true
      if (unchecked.length > 1) {
        this.searchedTags[categoryIndx].category.isSelected = false;
      }
      else {
        this.searchedTags[categoryIndx].category.isSelected = true;
      }
    }
    else {
      let index = this.kid.interestInfo.findIndex(x => x._id === this.searchedTags[categoryIndx].tags[tagIndex]._id)
      if (index !== -1) {
        this.kid.interestInfo.splice(index, 1)
      }
      this.searchedTags[categoryIndx].tags[tagIndex].isSelected = false
      this.searchedTags[categoryIndx].category.isSelected = false;
    }
  }

  formatSubtitle = (percent: number) => {
    return `${this.finished}/${this.total}`;
  }
  validateForm() {
    var d = 1;
    var m = document.forms["myForm"]["month"].value;
    var y = document.forms["myForm"]["year"].value;
    if (m == null || m == "") {
      alert("Please select date.");
      return false;
    }
    if (y == null || y == "") {
      alert("Please select date.");
      return false;
    }
    if ((m == 4 || m == 6 || m == 9 || m == 11) && d == 31) {
      alert("Selected month contains only 30 days.");
      return false;
    }
    if (m == 2 && d > 29 && (y % 4 == 0)) {
      alert("Selected month contains only 29 days.");
      return false;
    }

    if ((m == 2) && d > 28) {
      alert("Selected month contains only 28 days.");
      return false;
    }
    return true;
  }

  selectedCategory(category) {
    this.selectedCatId = category.id
    this.tags = category.tags.sort((a,b) => a.name > b.name ? 1 : -1);
  }

  ngOnDestroy(): void {
    this.subscribeUser.unsubscribe();
  }

  privacyPolicy() {
    const url = this.router.serializeUrl(
      this.router.createUrlTree(['/privacy-policy'])
    );

    window.open(url, '_blank');
  }
}

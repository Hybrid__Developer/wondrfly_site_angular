import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnboardingRoutingModule } from './onboarding-routing.module';
import { LoginParentComponent } from './login-parent/login-parent.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { AgmCoreModule } from '@agm/core';
import { CoreModule } from 'src/app/core/core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LandingNewComponent } from '../landing-pages/landing-new/landing-new.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import { MailchimpSubscribeFormModule } from 'src/app/core/shared/shared-components/mailchimp-subscribe-form/mailchimp-subscribe-form.module';
import { LoginProviderComponent } from './login-provider/login-provider.component';
import { ParentLandingComponent } from './parent-landing/parent-landing.component';

@NgModule({
  declarations: [LoginParentComponent, LandingNewComponent, LoginProviderComponent, ParentLandingComponent],
  imports: [
    CommonModule,
    OnboardingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MailchimpSubscribeFormModule,
    CoreModule,
    AgmCoreModule,
    DragScrollModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
    })
  ]
})
export class OnboardingModule { }

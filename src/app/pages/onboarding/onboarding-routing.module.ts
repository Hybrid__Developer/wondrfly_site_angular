import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from 'src/app/core/guards';
import { Role } from 'src/app/core/models/role.model';
import { LoginParentComponent } from './login-parent/login-parent.component';
import { LoginProviderComponent } from './login-provider/login-provider.component';
import { ParentLandingComponent } from './parent-landing/parent-landing.component';

const routes: Routes = [
  {
    path: '', component: LoginParentComponent, canActivate:
      [UserGuard],
    data: {
      roles: [
        Role.Parent,
      ]
    }
  },
  {
    path:'parent-new' , component:ParentLandingComponent
  },
  {
    path: 'login-provider', component: LoginProviderComponent,
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OnboardingRoutingModule { }

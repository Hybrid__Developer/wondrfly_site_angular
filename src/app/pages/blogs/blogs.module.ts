import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogPageComponent } from './blog-page/blog-page.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { RouterModule } from '@angular/router';
import { BlogsRoutes } from './blogs.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogSearchComponent } from './blog-search/blog-search.component';
import { BlogsComponent } from './blogs.component';
import { MarkdownModule } from 'src/app/core/shared/markdown/markdown.module';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { MailchimpSubscribeFormModule } from 'src/app/core/shared/shared-components/mailchimp-subscribe-form/mailchimp-subscribe-form.module';
import { ReadMoreModule } from 'src/app/core/shared/shared-components/read-more/read-more.module';
import { CoreModule } from "../../core/core.module";
@NgModule({
    declarations: [BlogPageComponent, BlogDetailComponent, BlogSearchComponent, BlogsComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(BlogsRoutes),
        ReactiveFormsModule,
        FormsModule,
        ReadMoreModule,
        MarkdownModule,
        NgxUiLoaderModule,
        MailchimpSubscribeFormModule,
        CoreModule
    ]
})
export class BlogsModule { }
